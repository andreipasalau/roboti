
const int trigPin1 = 2;
const int echoPin1 = 3;

const int trigPin2 = 4;
const int echoPin2 = 5;

const int trigPin3 = 6;
const int echoPin3 = 7;


const int touchPin1 = 12;
const int touchPin2 = 11;
const int touchPin3 = 10;

long duration;
uint8_t cm;
int incomingByte = 0;   // for incoming serial data 
 
void setup() {
  // initialize serial communication:
  Serial.begin(9600);
 
  pinMode(trigPin1, OUTPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(trigPin3, OUTPUT);
  
  pinMode(echoPin1, INPUT);
  pinMode(echoPin2, INPUT);
  pinMode(echoPin3, INPUT);
  
  pinMode(touchPin1, INPUT);
}
 
void loop()
{
  // send data only when you receive data:
         if (Serial.available() > 0) {
                 // read the incoming byte:
                 incomingByte = Serial.read();
                 
                 if (incomingByte == '1')
                 {
                   //digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
                   digitalWrite(trigPin1, LOW);
                   delayMicroseconds(2);
                   digitalWrite(trigPin1, HIGH);
                   delayMicroseconds(10);
                   digitalWrite(trigPin1, LOW);
                   duration = pulseIn(echoPin1, HIGH);
                   if(duration > 230 * 29 * 2) {
                    duration = 230 * 29 * 2;
                   }
                   cm = duration / 29 / 2;
                   Serial.write(cm);
                 }
                 else if (incomingByte == '2')
                 {
                   digitalWrite(trigPin2, LOW);
                   delayMicroseconds(2);
                   digitalWrite(trigPin2, HIGH);
                   delayMicroseconds(10);
                   digitalWrite(trigPin2, LOW);
                   duration = pulseIn(echoPin2, HIGH);
                   if(duration > 230 * 29 * 2) {
                    duration = 230 * 29 * 2;
                   }
                   cm = duration / 29 / 2;
                   Serial.write(cm);
                 }
                 else if (incomingByte == '3')
                 {
                   digitalWrite(trigPin3, LOW);
                   delayMicroseconds(2);
                   digitalWrite(trigPin3, HIGH);
                   delayMicroseconds(10);
                   digitalWrite(trigPin3, LOW);
                   duration = pulseIn(echoPin3, HIGH);
                   if(duration > 230 * 29 * 2) {
                    duration = 230 * 29 * 2;
                   }
                   cm = duration / 29 / 2;
                   Serial.write(cm);
                 } 
                 else if (incomingByte == '4')
                 {
                   Serial.write((uint8_t)digitalRead(touchPin1));
                 }
		 else if (incomingByte == '5')
                 {
                   Serial.write((uint8_t)digitalRead(touchPin2));
                 }
  	         else if (incomingByte == '6')
                 {
                   Serial.write((uint8_t)digitalRead(touchPin3));
                 }
                 else
                 {
                   Serial.write((uint8_t)0);
                 }
         }
}
