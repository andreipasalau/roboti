include <gripper.scad>
use <mendel_misc.inc>
use <parametric_involute_gear_v5.0.scad>

module surubM3() {
	cylinder(r =M3_radius, h = 3 * M8_support_height, $fn=40);
}

module surubM8() {
	rotate([0, 90, 0])cylinder(r =M8_nut_hole_radius, h = 10 * M8_support_height, $fn=40);
}

V_length = 35;
V_length_2 = 30;
V_tail = 15;

module V() {
	union(){
		rotate([0, -25, 0]) cube([V_length, finger_width*2, finger_thick/5]);
		rotate([0, 5, 0]) cube([V_length, finger_width * 2, finger_thick/5]);
		translate([-V_tail,0, -finger_width/2 ]) cube([V_tail + 5, finger_width*2, finger_width]);
	}
}

module tava() {
	cube([380, 260, 15]);
	translate([-20,-20,8])cube([420, 300, 5]);
}


module gear2() {

//big_bearing_tshirt_plate();

union(){
	translate([0,-22,0]){
		rotate([-90,0,0]){
			big_bearing_tshirt();
		}
	}
	rotate([90,0,0]){
		big_bearing_tshirt();
	}
}
translate([0, 15, 0]) rotate([90,0,0]){
	//lego_gear();
}

translate([50, 20, 15]) rotate([-90, 0, 0]){
	motor_cap();
}

translate([0, 60, 0]) rotate([-90, 0, 0]){
	gear_M8_long();
}

translate([0, 60, 21]){
	union(){
		translate([-21, 0, -42]){
			support1();
		}
		translate([21, 0, 0]) rotate([0, 180,0 ]){
			support1();
		}
	}
}

module support1();

//gripper_support();

//bearing_support();

//big_bearing_tshirt();


	translate([ sheet_length / 2 +15, 0, (sheet_height + M8_support_height)])rotate([0, -90, 0])
	union(){U();
	translate([0, 0, -(bearing_thickness + wall)])bearing_support();}

	
	translate([sheet_length / 2 +10 - margin_width, 0, 0])union(){
		translate([11,-7,1.8 * finger_width])rotate([0,0,150])union(){
		finger();
		translate([finger_first_half_length + finger_second_half_length - 34, V_tail + 95, -finger_width/2])
		rotate([0,0,120])
		V();}
		translate([11,7,1.8 * finger_width])rotate([180,0,210])union(){
		finger();
		translate([finger_first_half_length + finger_second_half_length - 2* finger_width - 34, V_tail +  85, finger_width/2])
		rotate([180,0,120])
		V();}
		
		//translate([-360, -303, 1.8 * finger_width - finger_width/2])rotate([0,0,130])V();
		//translate([-360, 303, 1.8 * finger_width + finger_width/2])rotate([0,0,230])V();
	}
	translate([0, - sheet_width / 2, 0]) rotate([0, 0, 0])gripper_support();
	translate([0, sheet_width / 2, (sheet_height + M8_support_height) * 2]) rotate([180, 0, 0])gripper_support();

	translate([sheet_length / 2 + 4 - margin_width + 5,0,-M8_support_height/2]) surubM3();
	translate([5, sheet_width / 2 - 11,-M8_support_height/2])surubM3();
	translate([5,-sheet_width / 2 + 11,-M8_support_height/2])surubM3();
	translate([225,10,-M8_support_height/2])surubM3();
	translate([225,-10,-M8_support_height/2])surubM3();
	translate([120, 0, M8_support_height + sheet_height]) surubM8();

}

//translate([-770, -130, 10]) tava();

gear2();

//finger();
//V();