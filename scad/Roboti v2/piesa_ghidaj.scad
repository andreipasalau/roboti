include <gripper.scad>
use <mendel_misc.inc>
use <parametric_involute_gear_v5.0.scad>

module surubM3() {
	cylinder(r =M3_radius, h = 3 * M8_support_height, $fn=40);
}

module surubM8() {
	rotate([0, 90, 0])cylinder(r =M8_nut_hole_radius, h = 20 * M8_support_height, $fn=40);
}

module surubM3Long() {
	rotate([0, 90, 0])cylinder(r =M3_radius, h = 20 * M8_support_height, $fn=40);
}

V_length = 35;
V_length_2 = 30;
V_tail = 15;

module V() {
	union(){
		rotate([0, -25, 0]) cube([V_length, finger_width*2, finger_thick/5]);
		rotate([0, 5, 0]) cube([V_length, finger_width * 2, finger_thick/5]);
		translate([-V_tail,0, -finger_width/2 ]) cube([V_tail + 5, finger_width*2, finger_width]);
	}
}

module tava() {
	cube([380, 260, 15]);
	translate([-20,-20,8])cube([420, 300, 5]);
}

module backend() {
	//union(){
		//translate([0,-22,0]){
			//rotate([-90,0,0]){
				//big_bearing_tshirt();
			//}
		//}
		//rotate([90,0,0]){
			//big_bearing_tshirt();
		//}
	//}

	//translate([0, 15, 0]) rotate([90,0,0]){
		//lego_gear();
	//}

	//translate([50, 20, 15]) rotate([-90, 0, 0]){
		//motor_cap();
	//}

	//translate([0, 60, 0]) rotate([-90, 0, 0]){
		//gear_M8_long();
	//}

	//translate([0, 60, 21]){
		//union(){
			//translate([-19.5, 0, -42]){
				//support1();
			//}
			//translate([19.5, 0, 0]) rotate([0, 180,0 ]){
				//support1();
			//}
		//}
	//}
}

module piesa_de_ghidaj(){
	square_length = 30;
	square_width = 28;
	square_height= 31;
	plate_verge=80.36;
	outside_distance = 95.2;	
	large_bearing_external_radius = 31;
	small_square_height = 20.2;
	hole_diameter = 20.98;
	
	union(){
		big_bearing_tshirt_plate();
		translate([large_bearing_external_radius / 2 - 2, 12, 0])
			difference()
			{
				cube(size = [square_length, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_height + 1, r = hole_diameter / 2);
			}

		translate([- large_bearing_external_radius / 2 - square_length + 2, 12, 0]) 
			difference(){ 
				cube(size = [square_length, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_height + 1, r = hole_diameter / 2);
			}

		translate([-large_bearing_external_radius / 2 - square_length + 2, - square_height - 12, 0]) 
			difference(){
				cube(size = [small_square_height, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_length, r = hole_diameter / 2);
			}

		translate([-large_bearing_external_radius / 2 - square_length + 2, -square_height / 2, 0]) 
			difference(){
				cube(size = [small_square_height / 3, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_length, r = hole_diameter / 2);
			}
	}
}

module inside_of_bearing_element(){
	inside_radius = 9;
	outside_radius = 19;
	thickness = 15.29;
	//M5_radius

	difference(){
		cylinder(h=thickness, r = outside_radius);

		translate([0, 0, -2]) {
			cylinder(h=thickness, r = inside_radius);
		}

		cylinder(h=thickness, r = inside_radius - 2);

		translate([(outside_radius - inside_radius)/2 + inside_radius,0,0]){
			cylinder(h=thickness, r = M5_radius);
		}

		translate([-(outside_radius - inside_radius)/2 - inside_radius,0,0]){
			cylinder(h=thickness, r = M5_radius);
		}

		translate([0,(outside_radius - inside_radius)/2 + inside_radius,0]){
			cylinder(h=thickness, r = M3_radius);
		}

		translate([0,-(outside_radius - inside_radius)/2 - inside_radius,0]){
			cylinder(h=thickness, r = M3_radius);
		}
	}

	piece_cap_thickness = 2;	

	translate([0, 0, -5]){
		difference(){
			cylinder(h=piece_cap_thickness, r = outside_radius);
			translate([0, 0, -2]) {
				cylinder(h=thickness, r = inside_radius - 2);
			}

			translate([(outside_radius - inside_radius)/2 + inside_radius,0,0]){
				cylinder(h=thickness, r = M5_radius);
			}

			translate([-(outside_radius - inside_radius)/2 - inside_radius,0,0]){
				cylinder(h=thickness, r = M5_radius);
			}

			translate([0,(outside_radius - inside_radius)/2 + inside_radius,0]){
				cylinder(h=thickness, r = M3_radius);
			}

			translate([0,-(outside_radius - inside_radius)/2 - inside_radius,0]){
				cylinder(h=thickness, r = M3_radius);
			}
		}
	}
}

module bumper_support(){
	support_inner_width = 7.78;
	support_height = 14.5;
	support_length = 70;
	margin_thickness = 2;
	margin_support_thickness = 13;
	cut_length = 28;
	total_thickness = 2 * margin_thickness + support_inner_width;

	difference(){
		cube(size = [support_length, support_inner_width + 2 * margin_thickness, support_height], center =false);
		translate([0, margin_thickness, 0]) cube(size = [cut_length, support_inner_width, support_height], center =false);

		translate([cut_length + 1, total_thickness / 2, support_height / 2  + 1]){
			rotate([0, 90, 0]) 	cylinder(h = support_length - cut_length, r = M5_radius);
		}
	}
}

module gear2() {

//big_bearing_tshirt_plate();

//gear_M8_long();

	//translate([ sheet_length / 2 +15, 0, (sheet_height + M8_support_height)])rotate([0, -90, 0])
		//union(){
			//U();
			//translate([0, 0, -(bearing_thickness + wall)])
			//bearing_support();
		//}

	//inside_of_bearing_element();
	//translate([sheet_length / 2 +10 - margin_width, 0, 0])union(){
		//translate([11,-7,1.8 * finger_width])rotate([0,0,150])union(){
		//finger();
		//translate([finger_first_half_length + finger_second_half_length - 34, V_tail + 95, -finger_width/2])
		//rotate([0,0,120])
		//V();}
		//translate([11,7,1.8 * finger_width])rotate([180,0,210])union(){
		//finger();
		//translate([finger_first_half_length + finger_second_half_length - 2* finger_width - 34, V_tail +  85, finger_width/2])
		//rotate([180,0,120])
		//V();}
		
		//translate([-360, -303, 1.8 * finger_width - finger_width/2])rotate([0,0,130])V();
		//translate([-360, 303, 1.8 * finger_width + finger_width/2])rotate([0,0,230])V();
	//}
	
	//translate([0, - sheet_width / 2, 0]) rotate([0, 0, 0])
	//gripper_support();
	//translate([0, sheet_width / 2, (sheet_height + M8_support_height) * 2]) rotate([180, 0, 0])
	//gripper_support();

	//translate([sheet_length / 2 + 4 - margin_width + 5,0,-M8_support_height/2]) surubM3();
	//translate([5, sheet_width / 2 - 11,-M8_support_height/2])surubM3();
	//translate([5,-sheet_width / 2 + 11,-M8_support_height/2])surubM3();
	//translate([175,10,-M8_support_height/2])surubM3();
	//translate([175,-10,-M8_support_height/2])surubM3();
	//translate([90, 0, M8_support_height + sheet_height]) surubM8();
	//translate([90, 0, M8_support_height / 2]) surubM3Long();
	//translate([90, 0, M8_support_height * 1.5 + 2 * + sheet_height]) surubM3Long();
}

//translate([-770, -130, 10]) tava();
//gear2();
piesa_de_ghidaj();

//bumper_support();

//translate([240,0,23])rotate([0,90,-90])backend();
//backend();
//finger();
//V();
//support1();