module big_bearing_tshirt_plate(large_bearing_internal_radius, large_bearing_external_radius, large_bearing_thickness)
{
	large_wall = 3;
	k = 4;
	difference (){
		translate ([-large_bearing_external_radius - k * large_wall, -large_bearing_external_radius - k * large_wall, 0])  cube ([2 * (large_bearing_external_radius + k * large_wall), 2 * (large_bearing_external_radius + k * large_wall), 2*large_wall]);
		cylinder (r = large_bearing_external_radius - large_wall, h = large_bearing_thickness + 1, $fn = 100);
	}	
}

module bloodySquare(sLength, sWidth, sHeight, sRadius, isSquare){
	difference()
			{
				cube(size = [sLength, sWidth, sHeight], center =false);
				if(isSquare == false){
				translate([sLength/2, sWidth/2, 0]) cylinder(h = sHeight, r = sRadius);
}
				else{
					translate([sLength/2, sWidth/2, 0]) cylinder(h = sHeight, r = sRadius, $fn=6);
				}
			}
}

module square_plate(size, thickness, radius){
	difference(){
		cube([size, size, thickness]);
		translate([size/2, size/2, 0])	cylinder(h = thickness, r = radius);
	}
}

module piesa_de_ghidaj2(){
	square_length = 30;
	square_width = 28;
	square_height= 31;
	hole_diameter = 21;
	offset = 4.5;
	outside_size = 0;

	big_square_size = 100 + 2 * 6.3;
	large_bearing_external_radius = 31;

	upperLeftX = big_square_size / 2 - square_length + outside_size;
	upperLeftZ = big_square_size / 2 - square_height;

	bottomLeftX = big_square_size / 2 - square_length + outside_size;
	bottomLeftZ = -big_square_size / 2;

	bottomRightX = -big_square_size / 2 - outside_size;
	bottomRightZ = -big_square_size / 2;

	bottom_center_cube_length= big_square_size - 2 * square_length;

	left_center_cube_height = big_square_size - 2 * square_height;

	union(){
		translate([-big_square_size / 2, 0, big_square_size/2]) rotate([-90,0,0]) square_plate(big_square_size, 6, large_bearing_external_radius);
		//top left
		translate([upperLeftX , offset, upperLeftZ]) bloodySquare(square_length, square_width, square_height, hole_diameter/2, false);
		//bottom left
		translate([bottomLeftX , offset, bottomLeftZ]) bloodySquare(square_length, square_width, square_height, hole_diameter/2, false);
		//bottom right
		translate([bottomRightX , offset, bottomRightZ]) bloodySquare(square_length, square_width, 17, hole_diameter/2, true);
		translate([-bottom_center_cube_length / 2, offset, - big_square_size / 2]) bloodySquare(bottom_center_cube_length, square_width, 10, hole_diameter/2, true);

//		translate([-bottom_center_cube_length / 2, 0, - big_square_size / 2]) 
		translate([big_square_size / 2 - 4, offset, -big_square_size/2]) cube([5, square_width, big_square_size]);
	}
}

module piesa_de_ghidaj(){
	large_bearing_internal_radius = 17;
	large_bearing_external_radius = 31;
	large_bearing_thickness = 5; 
	square_length = 30;
	square_width = 28;
	square_height= 31;
	plate_verge=80.36;
	outside_distance = 95.2;	
	large_bearing_external_radius = 31;
	small_square_height = 20.2;
	hole_diameter = 21;
	offset = 3;
	union(){
		big_bearing_tshirt_plate(large_bearing_internal_radius, large_bearing_external_radius, large_bearing_thickness);
		//top left
		translate([large_bearing_external_radius / 2 - 2, 17, offset])
			difference()
			{
				cube(size = [square_length, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_height + 1, r = hole_diameter / 2);
			}

		//bottom left
		translate([- large_bearing_external_radius / 2 - square_length + 2, 17, offset]) 
			difference(){ 
				cube(size = [square_length, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_height + 1, r = hole_diameter / 2);
			}

		//bottom right
		translate([-large_bearing_external_radius / 2 - square_length + 2, - square_height - 17, offset]) 
			difference(){
				cube(size = [small_square_height, square_height, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_length, r = hole_diameter / 2);
			}

		translate([-large_bearing_external_radius / 2 - square_length + 2, -square_height / 2 - 2, offset]) 
			difference(){
				cube(size = [small_square_height / 3, square_height + 4, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_length, r = hole_diameter / 2);
			}
/*
		translate([-large_bearing_external_radius / 2 - square_length + 2, -square_height / 2 - 2, offset]) 
			difference(){
				cube(size = [small_square_height / 3, square_height + 4, square_width], center =false);
				translate([-1, square_height / 2, square_width / 2]) rotate([0,90,0]) cylinder(h = square_length, r = hole_diameter / 3);
			}
*/
	}
}

piesa_de_ghidaj2();

//bloodySquare(30, 28, 31, 21/2);