module bloodySquare(sLength, sWidth, sHeight, sRadius, isSquare){
	difference()
			{
				cube(size = [sLength, sWidth, sHeight], center =false);
				if(isSquare == false){
				translate([sLength/2, sWidth/2, 0]) cylinder(h = sHeight, r = sRadius);
}
				else{
					translate([sLength/2, sWidth/2, 0]) cylinder(h = sHeight, r = sRadius, $fn=6);
				}
			}
}

module square_plate(width, height, thickness, radius){
	difference(){
		cube([width, height, thickness]);
		translate([width/2, height/2, 0])	cylinder(h = thickness, r = radius);
	}
}

module piesa_de_ghidaj3(){
	square_length = 30;
	square_width = 28;
	square_height= 31;
	hole_diameter = 21;
	offset = 4.5;
	outside_size = 0;

	big_square_height = 80;
	big_square_size = 100 + 2 * 6.3;
	large_bearing_external_radius = 31;

	screw_nut_radius = 11;

	upperLeftX = big_square_size / 2 - square_length + outside_size;
	upperLeftZ = big_square_height / 2 - square_height;

	bottomLeftX = big_square_size / 2 - square_length + outside_size;
	bottomLeftZ = -big_square_height / 2;

	bottomRightX = -big_square_size / 2 - outside_size;
	bottomRightZ = -big_square_height / 2;

	bottom_center_cube_length= big_square_size - 2 * square_length;

	left_center_cube_height = big_square_size - 2 * square_height;

	union(){
		translate([-big_square_size / 2, 0, big_square_height/2]) rotate([-90,0,0]) square_plate(big_square_size, big_square_height, 6, large_bearing_external_radius);
		//top left
		translate([upperLeftX , offset, upperLeftZ]) bloodySquare(square_length, square_width, square_height, hole_diameter/2, false);
		//bottom left
		translate([bottomLeftX , offset, bottomLeftZ]) bloodySquare(square_length, square_width, square_height, hole_diameter/2, false);
		//bottom right
		translate([bottomRightX , offset, bottomRightZ]) bloodySquare(square_length, square_width, 17, screw_nut_radius, true);
		translate([-bottom_center_cube_length / 2, offset, - big_square_height / 2]) bloodySquare(bottom_center_cube_length, square_width, 10, hole_diameter/2, true);

//		translate([-bottom_center_cube_length / 2, 0, - big_square_size / 2]) 
		translate([big_square_size / 2 - 4, offset, -big_square_height/2]) cube([5, square_width, big_square_height]);
	}
}

piesa_de_ghidaj3();