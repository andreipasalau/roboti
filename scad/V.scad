length = 50;
edge_thickness = 1.3;
width = 20;
M3_radius = 1.5;
extra_margin = 3;
height = 17 + 2 * extra_margin;

module slim_bar(){
	translate([0,-edge_thickness,0]) cube([length ,width + 20,edge_thickness]);
}

module slim_bar_middle(){
	translate([0,-edge_thickness -10,0]) cube([length , width + 2, 2]);
}

module hardener_half_piece(){
	inner_height = height - 2*(edge_thickness + extra_margin);
	offset = height-inner_height/2;
	union(){
	difference(){
		cube([length, width, height]);
		translate([0,0, extra_margin]){
			slim_bar();
		}
		cube([length ,width + 2,edge_thickness + 4]);

		translate([0,0,height/2 - edge_thickness/2 - 0.5]){
			slim_bar_middle();
		}

		translate([0,0,height - extra_margin - edge_thickness]){
			slim_bar();
		}

 		translate([0,0,height - extra_margin - edge_thickness - 1])	cube([length ,width + 2,edge_thickness + 5]);
	
	}

		//translate([0, width, height/4 -0.5]) cube([length, width/2, height / 2 + 1]);
	}
}

module holes(){
	cylinder(h = height, r=M3_radius, $fn=30);
}

module arm_hardener(){
	difference(){
		union(){
			//hardener_half_piece();
			translate([-length / 2,0,0]) hardener_half_piece();
			translate([-length / 2, 15, 11]) rotate([25, 0, 0])  cube([length, 45, 5]);
			translate([-length / 2, 15, 5.5]) rotate([0, 0, 0])  cube([length, 45, 5]);

//			translate([-length/2,length+5, 10]) cube([length, 5, 4]);
		//	translate([-length/2,length+2 + 3, 11]) 	rotate([0, 90, 0])	cylinder(h=length, r = 5, $fn = 30);
		}
		translate([-length/4 - 2, width/2 - 6.5, 0]) holes();
		translate([length/4 - 2, width/2 - 6.5, 0]) holes();
	}
}

//nice name... eh???:)
translate([0,0,-5]){
	arm_hardener();
}

//translate([0, 380, 0])rotate([0, 0, 180])
//translate([0,0,-5]){
//	arm_hardener();
//}
module tava() {
	cube([380, 260, 30]);
	translate([-20,-20,0])cube([420, 300, 20]);
}

//translate([0, 55, 0])tava();
