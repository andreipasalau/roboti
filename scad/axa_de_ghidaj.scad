

latime_ghidaj = 29.05;
lungime_ghidaj = 104;
inaltime = 6;
latime_cub_mic = 6.1;
lungime_cub_mic = 4.25;
raza = 6.05;
latime_piulita = 3;
raza_piulita = 2;
raza_piulita_interior = 10.5;
distanta_cerc_patrat = 37.2;

module ghidaj_support()
{
        difference(){
                translate ([-lungime_ghidaj/2,-latime_ghidaj/2,0]) cube ([lungime_ghidaj, latime_ghidaj, inaltime], center = false);
                cylinder (r = raza, h = inaltime, center = false, $fn = 50);
				translate([22, 0, 0]) cylinder (r = 4, h = inaltime, center = false, $fn = 50);
				translate([-22, 0, 0]) cylinder (r = 4, h = inaltime, center = false, $fn = 50);
                translate([distanta_cerc_patrat + raza, -latime_cub_mic/2, 0]) cube([lungime_cub_mic, latime_cub_mic, inaltime], center = false);
                translate([-distanta_cerc_patrat - raza - lungime_cub_mic, -latime_cub_mic/2, 0]) cube([lungime_cub_mic, latime_cub_mic, inaltime], center = false);
                translate([0,raza-1, inaltime /2])rotate([-90,0,0]) cylinder(h = latime_ghidaj /2 - raza +1, r = raza_pulita_interior, $fn = 30);
					 translate([0, raza + (latime_ghidaj/2 - raza)/2 + latime_piulita/2, 2]){
					 	 piulita();
					 }
        }
}

module piulita(){
	difference(){
		rotate([90, 0, 0])
		hull(){
			cylinder(h=latime_piulita, r=raza_piulita, $fn=6);
			translate([0, 5, 0]) cylinder(h= latime_piulita, r=raza_piulita, $fn=6);
		}
	}
	
}

ghidaj_support();