include <gripper.scad>

L=40;
high=45;
l=18;

M5_radius = 2.5;
bearing_external_radius = 11;

module backPiece() {

	difference(){
		cube([L, l, high]);
		
		translate ([L-9.5 + M3_radius, 5 + 1.5, -0.5]) cylinder (r = M3_radius+ tolerance + 0.5, h = high + 1, $fn = 30 );
		translate ([9.5 - M3_radius, 5 + 1.5, -0.5]) cylinder (r = M3_radius + tolerance + 0.5, h = high + 1, $fn = 30 );

		translate ([L/2, -0.5, high/2]) rotate([-90,0,0]) cylinder (r = 5, h = l + 1, $fn = 100);
// m5 support
		translate ([L/2, -0.5, high - 9]) rotate([-90,0,0]) cylinder (r = 2.5, h = l + 1, $fn = 100);
		translate ([L/2, -0.5, 9]) rotate([-90,0,0]) cylinder (r = 2.5, h = l + 1, $fn = 100);
	}
}


backPiece();