length = 15;
edge_thickness = 1.3;
width = 12;
M3_radius = 1.5;
extra_margin = 3;
height = 17 + 2 * extra_margin;

module slim_bar(){
	translate([0,-edge_thickness,0]) cube([length ,width + 2,edge_thickness]);
}

module slim_bar_middle(){
	translate([0,-edge_thickness -2,0]) cube([length , width + 2, 2]);
}

module hardener_half_piece(){
	inner_height = height - 2*(edge_thickness + extra_margin);
	offset = height-inner_height/2;
	union(){
	difference(){
		cube([length, width, height]);
		translate([0,0, extra_margin]){
			slim_bar();
		}
		cube([length ,width + 2,edge_thickness + 4]);

		translate([0,0,height/2 - edge_thickness/2 - 0.5]){
			slim_bar_middle();
		}

		translate([0,0,height - extra_margin - edge_thickness]){
			slim_bar();
		}

 		translate([0,0,height - extra_margin - edge_thickness - 1])	cube([length ,width + 2,edge_thickness + 5]);
	
	}

		translate([0, width, height/4 -0.5]) cube([length, width/2, height / 2 + 1]);
	}
}

module holes(){
	cylinder(h = height, r=M3_radius);
}

module arm_hardener(){
	difference(){
		union(){
			rotate([0,0,30]) hardener_half_piece();
			translate([-length,0,0]) hardener_half_piece();
		}
		translate([-length/2 - 2, width/2 - 1.5, 0]) holes();
		rotate([0,0,30]) translate([length/2 + 2, width/2 - 1.5, 0]) holes();
	}
}

//nice name... eh???:)
difference(){
translate([0,0,-5]){
	arm_hardener();
}

translate([-15,12.5,0]) rotate([0,0,15])cube([2*length, width, height / 2 + 2]);
}