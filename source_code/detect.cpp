#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <createoi.h>

// see http://stackoverflow.com/questions/7731742/square-detection-doesnt-find-squares/7732392#7732392
// and http://stackoverflow.com/questions/8667818/opencv-c-obj-c-detecting-a-sheet-of-paper-square-detection

using namespace cv;
using namespace std;

void find_squares(Mat& image, vector<vector<Point> >& squares);

typedef struct
{
	unsigned char hue, saturation, value;
} Color;

int extractColor ();
void moveCreate (int Xpos);
void setFloorHistogram ();
CvScalar hsv2rgb (float hue);
int findFloor();
void printSensors();

const float CAM_FOV = 15.0;
const int FRAME_WIDTH 	= 10;
const int FRAME_HEIGHT 	= 6;
const int NO_COLOR		= -2147483647;
const int STOP		= -2147483643;
CvHistogram* hist;
Color targetColorLower = {0, 0, 120};
Color targetColorUpper={250,250,255};
IplImage *frame, *huePlane, *satPlane, *valPlane, *imgHSV, *backproj, *smooth;
int hdims[3] = {12, 8, 4};
float **hranges;
IplImage** all_channels;
int yThreshold, key;
byte createIsRunning = 0;

int ALLOWED_MISSES_CONST = 250;
int allowedMisses = 0;
int lastFoundPointX, lastFoundPointY;

void horn() {
  char song[10 * 2];
  int i;
  for (i =0; i< 10; i++) {
    song[i*2] = 31 + floor(drand48() * (127-31));
    song[i*2 +1] = 8 + floor(drand48() * 10);
  }
  writeSong(0, 10, (byte*) song);
  playSong (0);
}

/** Main method */
int main(int argc, char* argv[])
{
	//if (argc < 2)
	//{
	//	printf ("Give location of serial port\n");
	//	return 1;
	//}
	int avgX, i;
	yThreshold = FRAME_HEIGHT / 4;

	int started=startOI ("/dev/ttyUSB0");
	if(started <0){
		printf("could not start robo\n");
//		return 1;
	}
	else{
		printf("started\n");
	}
	enterSafeMode();

	CvCapture* camera;

	if (0 == (camera = cvCaptureFromCAM (1)))
	{
	  fprintf (stderr, "Could not connect to camera\n");
	  return -1;
	}


	cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

	frame = cvQueryFrame (camera);
	imgHSV = cvCreateImage (cvGetSize (frame), 8, 3);
	smooth = cvCreateImage (cvGetSize (frame), 8, 3);




	huePlane = cvCreateImage (cvGetSize (frame), 8, 1);
	satPlane = cvCreateImage (cvGetSize (frame), 8, 1);
	valPlane = cvCreateImage (cvGetSize (frame), 8, 1);
	backproj = cvCreateImage (cvGetSize (frame), 8, 1);

	cvNamedWindow ("Camera Image", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow ("Backprojection", CV_WINDOW_AUTOSIZE);

	all_channels = (IplImage**)malloc(3* sizeof(IplImage *));
	hranges = (float**)malloc(3* sizeof(float *));
	hranges[0] = (float*)malloc(3 * 2 * sizeof(float));
	for(i = 1; i < 3; i++){
	  hranges[i] = hranges[0] + i * 2;
	}
	hranges[0][0] = 0;
	hranges[0][1] = 180;
	hranges[1][0] = 0;
	hranges[1][1] = 255;
	hranges[2][0] = 0;
	hranges[2][1] = 255;

	hist = cvCreateHist( 3, hdims, CV_HIST_ARRAY, hranges, 1 );
	all_channels[0] = huePlane;
	all_channels[1] = satPlane;
	all_channels[2] = valPlane;
	setFloorHistogram ();


	while (1)
	{
		frame = cvQueryFrame (camera);
		cvCvtColor (frame, imgHSV, CV_BGR2HSV);
		cvSmooth (imgHSV, smooth, CV_GAUSSIAN, 11, 11, 0, 0);
		cvSplit (imgHSV, huePlane, satPlane, valPlane, 0 );
		all_channels[0] = huePlane;
		all_channels[1] = satPlane;
		all_channels[2] = valPlane;

		/* This assumes that we have a clear shot at getting to the object when we see its color.
	 	 * Not always true, so we'll have to monitor bumper state when driving.  If we don't see
		 * the object we want, then we'll just find some more floor and wander a bit. */
		avgX = extractColor ();
		//if (NO_COLOR == avgX)
		//	avgX = findFloor();
		moveCreate (avgX);

		key = cvWaitKey (10) & 255;
		if (32 == key)					//toggle Create's movement with spacebar
		{
			printf("createIsRunning: %d\n", createIsRunning);
			if (createIsRunning)
			{

				printSensors();
				directDrive (0, 0);
				createIsRunning = 0;
			}
			else
				createIsRunning = 1;
		}
		if(key>=37 && key<=40){
			int speed = 0;
			int turn = 0;
			switch(key){
			case 38:
				drive(50, 0);
				sleep(1000);
				createIsRunning = 0;
				break;
			case 40:
				drive(-50, 0);
				sleep(1000);
				createIsRunning = 0;
				break;
			case 37:
				drive(50, -50);
				sleep(1000);
				createIsRunning = 0;
				break;
			case 39:
				drive(50, -50);
				sleep(1000);
				createIsRunning = 0;
				break;
			}
			createIsRunning = 0;
			//drive(speed, turn);
		}
		if('h'==key){
			horn();
		}
		if (27 == key)							//quit on Escape key press
			break;

		cvShowImage ("Camera Image", frame);
 	}

	stopOI();
	cvDestroyAllWindows();
	cvReleaseImage (&smooth);
	cvReleaseImage (&imgHSV);
	cvReleaseImage (&huePlane);
	cvReleaseImage (&satPlane);
	cvReleaseImage (&valPlane);
	cvReleaseImage (&backproj);
	cvReleaseHist (&hist);
	//cvReleaseImage (&frame);
	cvReleaseCapture (&camera);
	return 0;
}

float modulVector(int x1,int y1,int x2,int y2){
	int a=x2*x2-x1*x1;
	int b=y2*y2-y1*y1;
	float modul=sqrt(a+b);
	//printf("Modulul lui A(%d,%d) si B(%d,%d)=%f\n",x1,y1,x2,y2,modul);
	return modul;

}

//computes the distance between 2 points
float distP(Point p1, Point p2){
	return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));	
}

//computes the area of a triangle
float computeArea(Point p1, Point p2, Point p3){
	float edge1 = distP(p1, p2), edge2 = distP(p1, p3), edge3 = distP(p2, p3);
	//using heron's formula
	float s = (edge1 + edge2 + edge3) / 2;
	return sqrt(s * (s - edge1) * (s - edge2) * (s - edge3));
}

//checks if a point is inside a rectangle
//precondition:
//	points.size() == 4
bool checkIfPointInside(vector<Point> points, Point p){
	float allowedError = 5; // 5 pixels	

	//I splitted the rectangle into 2 triangles
	float areaOfRectangle = computeArea(points[0], points[1], points[2]) + computeArea(points[0], points[2], points[3]);
	
	// using the 5th point we split the rectangle into 4 triangles
	float areaOfTriangles = 0;
	for(int i = 0; i < 4; i++){
		areaOfTriangles += computeArea(p, points[i % 4], points[(i + 1) % 4]);
	}

	if(areaOfTriangles - areaOfRectangle > allowedError){
		//punctul este in exterior
		return false;
	} 

	return true;
}

/** \brief	Gets new color and returns average X position
 *
 *  This function returns the average X position of the color in the image, which is needed
 *  for	the Create to move.  The center of the image has an X equal to 0.
 *
 *  \return	the average X position of the given color or NO_COLOR if it is not in the image
 */
int extractColor ()
{

	byte* img_ptr;
	int total_x = 0, total_y = 0;
	int x, y;
	Mat img(frame,true);
        vector<vector <Point> > squares;
        find_squares(img, squares);

	int xPos = 0, yPos=0, bottom = 0;
	printf("We have found %d squares\n",squares.size());
        for( size_t i = 0; i < squares.size(); i++ )
	{

		vector<Point> p = squares[i];
		int numfound = 0;
		int colorfound=0;
		int edge=0;
		int pixels=0;
		int xMax=0,xMin=FRAME_WIDTH,yMax=0,yMin=FRAME_HEIGHT;
		printf("Square number %d has the points \n",i);
		for ( int j = 0; j < 4; j++ ) {
			if(p[j].x>xMax){
				xMax=p[j].x;
			}
			if(p[j].x<xMin){
				xMin=p[j].x;
			}
			if(p[j].y>yMax){
				yMax=p[j].y;
			}
			if(p[j].y<yMin){
				yMin=p[j].y;
			}
			printf("point nr %d with x=%d and y=%d\n",j,p[j].x,p[j].y);
        		cvLine( frame, p[j], p[(j+1)%4], Scalar(0,255,0), 1, 8 ); // green
	        }
		//float modulAB=modulVector(p[0].x,p[0].y,p[1].x,p[1].y);
		//float modulAD=modulVector(p[0].x,p[0].y,p[3].x,p[3].y);
		printf("xmin=%d,xmax=%d,ymin=%d,ymax=%d\n",xMin,xMax,yMin,yMax);		
		for (y = yMin; y < yMax; y++)	{
			for (x = xMin; x < xMax; x++){
				pixels++;
				//float modulAM=modulVector(p[0].x,p[0].y,x,y);
				//float left=modulAM*modulAB;
				//float right=modulAM*modulAD;
				//if( left>0 && left<modulAB*modulAB && right>0 && right<modulAD*modulAD){
				Point currentPoint;
				currentPoint.x = x;
				currentPoint.y = y;
				if(checkIfPointInside(p, currentPoint)){
					//printf("Point %d,%d is inside rectangle\n",x,y);
					numfound++;
					img_ptr = &((byte*)(smooth->imageData + smooth->widthStep*y))[x*3];
					if(targetColorLower.hue<=img_ptr[0] && targetColorUpper.hue>=img_ptr[0] &&
					  targetColorLower.saturation<=img_ptr[1] && targetColorUpper.saturation>=img_ptr[1] &&
					   targetColorLower.value<=img_ptr[2] && targetColorUpper.value>=img_ptr[2]){
						colorfound++;
					//	printf("the point with x=%d and y=%d was the right color\n",x,y);
						if(y == smooth->height - 1) {
							edge++;
						}
					}
					
					
				}	
			}		
		}

		
		int centerX = (p[0].x + p[2].x) / 2;
		int centerY = (p[0].y+ p[2].y) / 2;
		printf("NUmfound=%d and pixels=%d colorfound=%d\n",numfound,pixels,colorfound);

		bool keepOldValues = false;

		if(numfound>0){
			float ratio=colorfound/(float)numfound;
			printf("THe rectangle number %d had %d/%d=%f\n",i,numfound,colorfound,ratio);
			if(ratio >= 0.45){
				printf("LPT: Reinit allowed misses.");
				lastFoundPointX = xPos = centerX;
				lastFoundPointY = yPos = centerY;
				bottom = edge;
				printf("xPOS=p[0].x=%d + p2.x=%d /2 =%d\n",p[0].x,p[2].x,xPos);
				printf("yPOS=p[0].y=%d + p2.y=%d /2 =%d\n",p[0].y,p[2].y,yPos);
				allowedMisses = ALLOWED_MISSES_CONST;
			}else if(allowedMisses > 0){
				keepOldValues = true;
			}
		}else if(allowedMisses > 0){
			keepOldValues = true;	
		}

		if(keepOldValues){
			printf("LPT: Took old values. Miss %d\n", allowedMisses);
			allowedMisses--;
			xPos = lastFoundPointX;
			yPos = lastFoundPointY;
		}
	}



	//for (y = 0; y < smooth->height; y++)/	{
	//	for (x = 0; x < smooth->width; x++)
	//	{
	//		img_ptr = &((byte*)(smooth->imageData + smooth->widthStep*y))[x*3];
//
//			if(targetColorLower.hue<=img_ptr[0] && targetColorUpper.hue>=img_ptr[0] &&
//			  targetColorLower.saturation<=img_ptr[1] && targetColorUpper.saturation>=img_ptr[1] &&
//			   targetColorLower.value<=img_ptr[2] && targetColorUpper.value>=img_ptr[2])
//			{
//				numfound++;
//				total_x += x;
//				total_y += y;
//				if(y == smooth->height - 1) {
//					bottom++;
//				}
//			}
//		}
//	}

	if (xPos == 0) {
		printf("No color found.\n");
		return NO_COLOR;
	}

	if(bottom >= 1) {
		return STOP;
	}

	cvLine (frame, cvPoint (xPos,yPos),
			cvPoint (FRAME_WIDTH / 2, FRAME_HEIGHT-1), CV_RGB (0, 255, 0), 2, 4, 0);

	//imshow("Camera Image", frame);

//	printf("total x=%d and numfound= %d\n",total_x,numfound);
 	return	xPos;
}

/** \brief	Directs Create's motion based on image info
 *
 *  Tells the Create to move based on the value of Xpos.  Create will try to move such that Xpos
 *  is close to 0.  If Xpos is NO_COLOR, then the Create will spin in a circle in an attempt to
 *  find the color.
 *
 *  \param	Xpos	X position of the object the Create should be following
 */
void moveCreate (int Xpos)
{
	int tol = FRAME_WIDTH / 6, bumper = 0;
	float angle;



	printf("Create is runing %d\n",createIsRunning);
	if (!createIsRunning)
		return;

	bumper = getBumpsAndWheelDrops();

	printf("%s, Xpos %d, bum %d\n", __func__, Xpos, bumper);
	if(STOP == Xpos) {
		directDrive (0, 0);
		createIsRunning = 0;
		printf("Found the object. :)))\n");
	}
	else if (0 != bumper)							//sensor was tripped
	{
		printf("Sensor tripped\n");
		if (1 == bumper)
		{
			drive (20, 1);
			waitAngle (45, 1);
		}
		else if (2 == bumper)
		{
			drive (20, -1);
			waitAngle (-45, 1);
		}
		else if (3 == bumper)
		{
			drive (50, -1);
			waitAngle (-90, 1);
		}
	}
	else if (NO_COLOR == Xpos){

		printf("no color found,i'm spinning\n");				//if object is not in frame, spin to find it
		drive (15, 1);
		}
	else
	{
		//todo stabilizare imagine
		 if (Xpos - (FRAME_WIDTH/2) < tol && Xpos - (FRAME_WIDTH/2) > -tol){
			printf("obj is straight ahead\n");
			//move toward object if it is straight ahead
			drive (200, 0);
		}
		else									//else turn toward object
		{
			printf("turning toward the obj\n");
			if(Xpos == (FRAME_WIDTH/2)) {
				angle = 0;
			} else {
				Xpos=Xpos-(FRAME_WIDTH/2);
				angle = -CAM_FOV / ((FRAME_WIDTH / 2) / Xpos);
			}
			printf("with angle=%f\n",angle);
			if (angle < 0){
				drive (120, 10*(int)angle);
			}
			else{
				drive (120, 14*(int)angle);
			}	
		}
	}
	return;
}

/** \brief  Set color histogram for floor
 *
 * 	Uses the lower portion of an image to set the histogram for the floor.  The function just uses the lower
 *  portion of the image to find it since we can assume that the floor will always be down there.
 */
void setFloorHistogram ()
{
	CvRect rect = cvRect (FRAME_WIDTH / 3, FRAME_HEIGHT - yThreshold,
			      FRAME_WIDTH / 3, yThreshold);

	cvSetImageROI (huePlane, rect);
	cvSetImageROI (satPlane, rect);
	cvSetImageROI (valPlane, rect);
	cvCalcHist (all_channels, hist, 0, 0);
	cvNormalizeHist (hist, 1024);
	cvResetImageROI (huePlane);
	cvResetImageROI (satPlane);
	cvResetImageROI (valPlane);
}

/** \brief Finds area with most floor ahead
 *
 *  Gets the backprojection of the current frame and uses that to find the X coordinate of the area where the
 *  floor extends furthest into the image frame.
 *
 *  \return 	X position of the region with the most floor space
 */
IplImage* GetThresholdedImage(IplImage* imgHSV){
       IplImage* imgThresh=cvCreateImage(cvGetSize(imgHSV),IPL_DEPTH_8U, 1);
       cvInRangeS(imgHSV, cvScalar(0,100,100,0), cvScalar(4,255,255,0), imgThresh);
       return imgThresh;
}
int findFloor ()
{

    	frame=cvCloneImage(frame);
    	cvSmooth(frame, frame, CV_GAUSSIAN,3,3,0,0); //smooth the original image using Gaussian kernel

    	IplImage* aux = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 3);
    	cvCvtColor(frame, aux, CV_BGR2HSV); //Change the color format from BGR to HSV
    	//IplImage* imgThresh = GetThresholdedImage(aux);

    	//cvSmooth(imgThresh, imgThresh, CV_GAUSSIAN,3,3,0,0); //smooth the binary image using Gaussian kernel

    	//cvShowImage("Backprojection", imgThresh);


	byte* img_ptr;
	int i, x, y, x_max = 0, y_max = FRAME_HEIGHT;

	for (i = 0; i < FRAME_WIDTH; i++)
	{
	  x = (FRAME_WIDTH / 2 + i) % FRAME_WIDTH;      //start in middle of image

	  for (y = FRAME_HEIGHT - 1; y > 0; y--)
	  {
	    img_ptr = &((byte*)(backproj->imageData + backproj->widthStep*y))[x];

	    if (*(img_ptr - 1) != 255)
	    {
			if (y < y_max)
			{
				x_max = x;
				y_max = y;
			}
			break;
	    }
	  }
	}
	cvLine (frame, cvPoint (x_max, y_max), cvPoint (FRAME_WIDTH / 2, FRAME_HEIGHT-1),
			CV_RGB (255, 0, 0), 2, 4, 0);
	return x_max - (FRAME_WIDTH / 2);
}

/** \brief	Converts hue to RGB value
 *
 * 	Converts a single hue value to an RGB value, which is stored in a CvScalar type.  Taken directly
 * 	from the OpenCV Camshift demo.
 *
 * 	\param	hue		Hue value to convert
 *
 * 	\return 	a CvScalar of the new RGB value.
 */
CvScalar hsv2rgb( float hue )
{
	int rgb[3], p, sector;
	static const int sector_data[][3]=
	{{0,2,1}, {1,2,0}, {1,0,2}, {2,0,1}, {2,1,0}, {0,1,2}};
	hue *= 0.033333333333333333333333333333333f;
	sector = cvFloor(hue);
	p = cvRound(255*(hue - sector));
	p ^= sector & 1 ? 255 : 0;

	rgb[sector_data[sector][0]] = 255;
	rgb[sector_data[sector][1]] = 0;
	rgb[sector_data[sector][2]] = p;

	return cvScalar(rgb[2], rgb[1], rgb[0],0);
}



void printSensors()
{
	int* sensors = getAllSensors();
	int i;

	free (sensors);
}
// helper function:
// finds a cosine of angle between vectors
// from pt0->pt1 and from pt0->pt2
double angle( Point pt1, Point pt2, Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

void find_squares(Mat& image, vector<vector<Point> >& squares)
{
    // blur will enhance edge detection
    // dilate(image, image, Mat(), Point(-1,-1));
    Mat blurred(image);
    medianBlur(image, blurred, 9);

    Mat gray0(blurred.size(), CV_8U), gray;
    vector<vector<Point> > contours;

    // find squares in every color plane of the image
    for (int c = 0; c < 3; c++)
    {
        int ch[] = {c, 0};
        mixChannels(&blurred, 1, &gray0, 1, ch, 1);

        // try several threshold levels
        const int threshold_level = 2;
        for (int l = 0; l < threshold_level; l++)
        {
            // Use Canny instead of zero threshold level!
            // Canny helps to catch squares with gradient shading
            if (l == 0)
            {
                Canny(gray0, gray, 10, 20, 3); //

                // Dilate helps to remove potential holes between edge segments
                dilate(gray, gray, Mat(), Point(-1,-1));
            }
            else
            {
                    gray = gray0 >= (l+1) * 255 / threshold_level;
            }

            // Find contours and store them in a list
            findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

            // Test contours
            vector<Point> approx;
            for (size_t i = 0; i < contours.size(); i++)
            {
                    // approximate contour with accuracy proportional
                    // to the contour perimeter
                    approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.02, true);

                    // Note: absolute value of an area is used because
                    // area may be positive or negative - in accordance with the
                    // contour orientation
                    if (approx.size() == 4 &&
                            fabs(contourArea(Mat(approx))) > 1000 &&
                            isContourConvex(Mat(approx)))
                    {
                            double maxCosine = 0;

                            for (int j = 2; j < 5; j++)
                            {
                                    double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                                    maxCosine = MAX(maxCosine, cosine);
                            }

                            if (maxCosine < 0.3)
                                    squares.push_back(approx);
                    }
            }
        }
    }
}

