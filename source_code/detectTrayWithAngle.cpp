#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

using namespace cv;
using namespace std;

const char* CAMERA_NAME = "cam_window";


log4cpp::Category* root;

const float CAM_FOV = 15.0;
const int FRAME_WIDTH 	= 640;
const int FRAME_HEIGHT 	= 480;
const int NO_COLOR		= -2147483647;
const int STOP		= -2147483643;

Point pOrigin;

typedef struct {
    Point p1, p2;
    double length;
    double angle;
} LineInfo;

typedef struct
{
    unsigned char hue, saturation, value;
} Color;

//Color targetColorLower = { 5, 50, 50 };
Color targetColorLower = {0,100,100};
//Color targetColorUpper = {15, 255, 255};
Color targetColorUpper={4,255,255};

void initLogger(){
    //init logging
    std::string initFileName = "log4cpp.properties";

    log4cpp::PropertyConfigurator::configure(initFileName);
    root = &(log4cpp::Category::getRoot());
}

CvCapture* loadCamera(const char* name, int index){
    CvCapture *camera = cvCaptureFromCAM(index);
    if(camera == 0){
        root->info("Could not read from camera.");
        return 0;
    }

    cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

    cvNamedWindow (name, CV_WINDOW_AUTOSIZE);
    cvMoveWindow(name, 500, 0);

    return camera;
}

IplImage* GetThresholdedImage(IplImage* imgHSV){       
       IplImage* imgThresh=cvCreateImage(cvGetSize(imgHSV),IPL_DEPTH_8U, 1);
       cvInRangeS(imgHSV, cvScalar(targetColorLower.hue, targetColorLower.saturation, targetColorLower.value),
       cvScalar(targetColorUpper.hue, targetColorUpper.saturation, targetColorUpper.value), imgThresh);
       return imgThresh;
} 


/*
 * Consider having the following segments: [P0, P1] and [P1, P2].
 * This method returns an number that has the following meaning:
 *      - if nr < 0 then in P1 you make a clockwise turn to reach P2
 *      - if nr > 0 then in P1 you make a counterclockwise turn to reach P2
 *      - else P0, P1 and P2 are on the same line. 
 */
int turns(cv::Point p0, cv::Point p1, cv::Point p2){
    return (p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y);
}

/*
 * This method extracts all points from the provided lines.
 */
std::vector<cv::Point> extractPoints(std::vector<Vec4i> lines){
    std::vector<cv::Point> points;

    for(unsigned int i = 0; i < lines.size(); i++){
        points.push_back(cv::Point(lines[i][0], lines[i][1]));
        points.push_back(cv::Point(lines[i][2], lines[i][3]));
    }

    return points;
}

int cmp(Point p1, Point p2) { 
    return turns(pOrigin, p1, p2) < 0;
}

/*
 * The Graham scanning algorithm to find a convex hull of a set of points.
 * Complexity: O(n*log2(n)) - given by the sorting algorithm.
 */
std::vector<cv::Point> executeGrahamScan(std::vector<cv::Point> points){
    std::vector<cv::Point> convexHullPoints;

    if(points.size() < 3){
        // If there aren't at least 3 points then there cannot be any convex polygon.
        //root->info("There weren't at least 3 points to start the Graham scan.");
        return convexHullPoints;
    }

    //extract origin
    uint pmin = 0;
    for(uint i = 0; i < points.size(); i++){
        if(points[i].y < points[pmin].y){
            pmin = i;
        }
    }

    swap(points[0], points[pmin]);
    pOrigin = points[0];

    //sort points
    sort(++(points.begin()), points.end(), cmp);
    
    //Graham scan algorithm
    convexHullPoints.push_back(points[0]);
    convexHullPoints.push_back(points[1]);

    for (uint i = 2; i < points.size(); ++i)
    {
        while (convexHullPoints.size()>=2 && turns(convexHullPoints[convexHullPoints.size() - 2], 
                                              convexHullPoints[convexHullPoints.size() -1], 
                                              points[i])>0 ) 
            convexHullPoints.pop_back();

        convexHullPoints.push_back(points[i]);
    }
    
    return convexHullPoints;
}

std::vector<Vec4i> extractConvexHullLines(std::vector<cv::Point> convexHullPoints) {
    std::vector<Vec4i> lines;

    for(uint i = 0; i < convexHullPoints.size() - 1; i++){
        lines.push_back(Vec4i(convexHullPoints[i].x, convexHullPoints[i].y, convexHullPoints[i+1].x, convexHullPoints[i+1].y));
    }
    uint last = convexHullPoints.size() - 1;
    lines.push_back(Vec4i(convexHullPoints[last].x, convexHullPoints[last].y, convexHullPoints[0].x, convexHullPoints[0].y));

    return lines;
}

inline double distanceBetween2Points(Point &p1, Point &p2){
    return sqrt( (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y)  );
}

double computeAngleOfLine(Point p1, Point p2){
    Point firstPoint = p1;
    Point secondPoint = p2;

    if(firstPoint.y > secondPoint.y){
        firstPoint = p2;
        secondPoint = p1;
    }

    double angle = atan2((double)secondPoint.y - firstPoint.y,
                        (double)secondPoint.x - firstPoint.x) * 180 / CV_PI;

    return angle;
}

double extractAngle(std::vector<Vec4i> lines){
    std::vector<LineInfo> lineInfos;

    for(unsigned int i = 0; i < lines.size(); i++){
        LineInfo lineInfo;
        lineInfo.p1 = Point(lines[i][0], lines[i][1]);
        lineInfo.p2 = Point(lines[i][2], lines[i][3]);
        lineInfo.length = distanceBetween2Points(lineInfo.p1, lineInfo.p2);
        lineInfo.angle = computeAngleOfLine(lineInfo.p1, lineInfo.p2);
        //lineInfo.angle = atan2((double)lines[i][3] - lines[i][1],
        //               (double)lines[i][2] - lines[i][0]) * 180 / CV_PI;

        lineInfos.push_back(lineInfo);
    }

    uint max = 0;

    for(uint i = 1; i < lineInfos.size(); i++){
        if(lineInfos[max].length < lineInfos[i].length){
            max = i;
        }
    }

    return lineInfos[max].angle;
}

inline double adjustAngle(double angle){
    return angle - 90;
}

void detectTray(CvCapture *camera, const char* windowName){
    const int ESC_KEY = 27;
    int anglecount = 0;
    double angle = 0.0, currentAngle = 0.0;
    int angleThreshold = 3;
    int key;
    IplImage *frame, *imgHSV;//, *smooth;//, *huePlane, *satPlane, *valPlane;
    root->info("============ >>> Detecting tray...");

    //init frames
    frame = cvQueryFrame(camera);
    imgHSV = cvCreateImage(cvSize(frame->width, frame->height), frame->depth, 3);
    
    bool trayNotFound = true;

    while(trayNotFound){
        
        frame = cvQueryFrame(camera);
        
        cvCvtColor(frame, imgHSV, CV_BGR2HSV);
        
        IplImage *temp = GetThresholdedImage(imgHSV);
        
        //cvNot(temp, temp);
        vector<Vec4i> lines;
        HoughLinesP(Mat(temp), lines, 1, CV_PI/180, 100, 10, 35);

        //root->info("Lines.size() == %d", lines.size());

        cv::Mat disp_lines(cvSize(frame->width, frame->height), CV_8UC1, cv::Scalar(0, 0, 0));
        
        unsigned nb_lines = lines.size();
        
        for(uint i = 0; i < nb_lines; i++){
            //root->info("line: Point1[%d, %d] and Point2[%d, %d]", lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
            cv::line(disp_lines, cv::Point (lines[i][0],lines[i][1]), cv::Point(lines[i][2], lines[i][3]), cv::Scalar(255, 0, 0));
            angle+=atan2((double)lines[i][3] - lines[i][1],
                       (double)lines[i][2] - lines[i][0]);
        }
        
        std::vector<cv::Point> points = extractPoints(lines);
        if(points.size() > 3){
            std::vector<cv::Point> convexHullPoints = executeGrahamScan(points);

            std::vector<Vec4i> convexHullLines = extractConvexHullLines(convexHullPoints);

            currentAngle = extractAngle(convexHullLines);
            currentAngle = currentAngle - 90;

            root->info("angle = %f", currentAngle);
            /*
            if(anglecount < angleThreshold){
                angle += currentAngle;
                anglecount++;
            }else{
                root->info("angle = %f", angle / angleThreshold);
                anglecount = angle = 0;
            }
            */
            if(convexHullPoints.size() > 2){
                for(unsigned int i = 0; i < convexHullPoints.size() - 1; i++){
                    //cv::line(disp_lines, cv::Point (convexHullPoints[i].x, convexHullPoints[i].y), cv::Point(convexHullPoints[i + 1].x, convexHullPoints[i + 1].y), cv::Scalar(230, 220, 15));
                    cvLine(frame, cv::Point (convexHullPoints[i].x, convexHullPoints[i].y), cv::Point(convexHullPoints[i + 1].x, convexHullPoints[i + 1].y), cv::Scalar(230, 220, 15));
                }

                cvLine(frame, cv::Point (convexHullPoints[convexHullPoints.size()-1].x, convexHullPoints[convexHullPoints.size()-1].y), cv::Point(convexHullPoints[0].x, convexHullPoints[0].y), cv::Scalar(230, 220, 15));
            }
        }
        
        /*        
        angle /= nb_lines;
        root->info("angle = %f", angle*180/CV_PI);
        */
        cv::imshow("test", disp_lines);

        cvShowImage("real", frame);
        cvShowImage(windowName, temp);
        cvReleaseImage(&temp);

        key = cvWaitKey(15);

        if(key == ESC_KEY){
            break;
        }
    }
}

void testGrahamScan(){
    std::vector<cv::Point> points;
    points.push_back(cv::Point(1, 1));
    points.push_back(cv::Point(1, 6));
    points.push_back(cv::Point(2, 5));
    points.push_back(cv::Point(3, 1));
    points.push_back(cv::Point(3, 4));
    points.push_back(cv::Point(4, 2));
    points.push_back(cv::Point(4, 8));
    points.push_back(cv::Point(5, 3));
    points.push_back(cv::Point(5, 7));
    points.push_back(cv::Point(6, 0));

    std::vector<cv::Point> convexHull = executeGrahamScan(points);
    root->info("Convex Hull points: =================================");
    for(unsigned int i = 0; i < convexHull.size(); i++){
        root->info("P(%d, %d)", convexHull[i].x, convexHull[i].y);
    }
}

void testAngle(){
    LineInfo line1, line2;
    Point p1(5, 1);
    Point p2(1, 4);
    //Point p3(8, 4);
    //Point p4(7.5, 6);

    line1.p1 = p1;
    line1.p2 = p2;
    line1.angle = computeAngleOfLine(line1.p1, line1.p2);
    root->info("line1: %f", line1.angle);

    /*
    line2.p1 = p2;
    line2.p2 = p4;
    line2.angle = atan2((double)line2.p2.y - line2.p1.y,
                       (double)line2.p2.x - line2.p1.x) * 180 / CV_PI;
    root->info("line2: %f", line2.angle);

    root->info("are almost perpendicular: %d", areAlmostPerpendicular(line1, line2));
    */
}

int main(){
    initLogger();

    //testGrahamScan();
    //testAngle();
    
    CvCapture *camera = loadCamera(CAMERA_NAME, 1);
    if(!camera) return -1;

    detectTray(camera, CAMERA_NAME);

    //close camera and logger
    cvDestroyAllWindows();
    cvReleaseCapture(&camera);
    
    log4cpp::Category::shutdown();

    return 0;
}