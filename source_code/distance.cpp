#include "distance.h"

#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <iostream>

Distance::Distance(char *serial)
{
    fd = open (serial, O_RDWR | O_NOCTTY);

    if (fd < 0)
    {
        perror ("Could not open serial port");
        return;
    }

    struct termios toptions;

    if (tcgetattr(fd, &toptions) < 0) {
        perror("init_serialport: Couldn't get term attributes");
        return;
    }

    speed_t brate = B9600; // baud rate
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;

    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 1;
    toptions.c_cc[VTIME] = 0;

    if( tcsetattr(fd, TCSANOW, &toptions) < 0) {
        perror("init_serialport: Couldn't set term attributes");
        return;
    }

    // reboot arduino
    usleep(2200 * 1000);
}

int Distance::read(int sensor)
{
    char sen = ((char)sensor) + 48;
    char buf[1] = { sen };
    int n = ::write (fd, &buf, 1);

    if (n < 0) {
        return -1;
    }
    usleep(20*1000);
    n = ::read( fd, buf, 1 );

    if (n < 0)
        return -1;

    // n = atoi(buf);

    return (unsigned char)buf[0];
}

void Distance::close()
{
    ::close(fd);
}
