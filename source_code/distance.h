#ifndef DISTANCE_H
#define DISTANCE_H

#include <termios.h>

class Distance
{
public:
    Distance(char *serial);
    int read(int sensor);
    void close();

private:
    int fd;
};

#endif // DISTANCE_H
