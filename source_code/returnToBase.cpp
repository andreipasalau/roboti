#include <createoi.h>
#include <stdlib.h>
#include <stdio.h>

typedef enum {
    AHEAD,
    LEFT,
    RIGHT,
    ROATE
} Direction;

Direction getRobotDirection(int code){
    switch(code){
        case 248:
        case 250:
            return LEFT;
        case 244:
        case 246:
            return RIGHT;
        case 252:
        case 254:
            return AHEAD;
    }

    return WONDER_AROUND;
}

int main()
{
    int started=startOI ("/dev/ttyUSB0");
    enterSafeMode();
    //runCoverAndDockDemo ();

    while(1){
        int sensorValue = readSensor ((oi_sensor)17);
        
        Direction dir = getRobotDirection(sensorValue);

        printf("sensor value: %d\n", sensorValue);
        if(sensorValue == 240) {
            printf("UNKNOWN\n");
        } else if(sensorValue == 248) {
            //fa stanga
            printf("Red buoy\n");   
        } else if(sensorValue == 244) {
            //fa dreapta
            printf("Green buoy\n");   
        } else if(sensorValue == 242) {
            printf("Force field\n");   
        } else if(sensorValue == 252) {
            printf("Red and gree buoy\n");   
        } else if(sensorValue == 250) {
            //fa stanga
            printf("Red buoy and Force field\n");   
        } else if(sensorValue == 246) {
            //fa dreapta
            printf("Green buoy and Force field\n");   
        } else if(sensorValue == 254) {
            printf("Red and green bouy and Force field\n");   
        } else {
            printf("Incercam metoda Marius\n");
        }
    }
    sleep(2);

    drive(0,0);
}

/*
int main()
{
    printf("hello world2.\n");
	int started = startOI ("/dev/ttyUSB0");
	if(started < 0)
    {
        root->info("could not start robo\n");
        //return -1;
    }
    else
    {
        root->info("robo started\n");
    }

    printf("portocale\n");

	enterSafeMode();
	int sensorValue = readSensor (SENSOR_INFRARED);

	printf("sensor value: %d\n", sensorValue);

	stopOI();

	return 0;	
}
*/