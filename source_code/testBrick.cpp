// NXT++ test.cpp : Defines the entry point for the console application.
//

#include "NXT++.h"
#include <iostream>

using namespace std;

int main()
{
    cout << "NXT++ test project" << endl;

    if(NXT::Open()) //initialize the NXT and continue if it succeds
	{
				NXT::Motor::SetForward(OUT_A, 50); //turn the motor in port 1 on 50% power
				sleep(1);
				NXT::Motor::Stop(OUT_A, false); //if the touch sensor is not pressed down turn the motor off
    }
	return 0;
}

