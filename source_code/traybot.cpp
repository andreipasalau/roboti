#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <signal.h>
#include "distance.h"
#include <iostream>
#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <createoi.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include "NXT++.h"
#include <deque>
#include <list>

using namespace cv;
using namespace std;

const char* CAMERA1_NAME = "Camera 1";
const char* CAMERA2_NAME = "Camera 2";
const int DISTANCE_TO_TABLE = 30;
const int DISTANCE_BETWEEN_SENSORS = 9;
const unsigned int DEQUE_SIZE = 7;
const unsigned int ANGLE_DEQUE_SIZE = 7;

CvCapture* camera1, *camera2;

int thresh = 100;
int max_thresh = 255;
//Random number generator
RNG rng(12345);

typedef struct {
    Point p1, p2;
    double length;
    double angle;
} LineInfo;

typedef struct {
    bool isSuccessful;
    double angle;
} AngleResult;

typedef enum {
    AHEAD,
    LEFT,
    RIGHT,
    ROTATE
} Direction;

typedef enum {
    STAND_BY,
    IN_LEFT_ROTATION,
    IN_RIGHT_ROTATION,
    IN_MARCH, // ahead
    IN_MINUS_180_ROTATION, //to left
    IN_PLUS_360_ROTATION //to right
} RoboState;

typedef struct
{
    unsigned char hue, saturation, value;
} Color;

typedef struct{
    bool foundAtLeast3Points, caughtTrayInImage;
    Point weightCenter;
    std::vector<Vec4i> convexHullLines;
    double angle;
} ComputeResult;

ComputeResult computeAngleData(IplImage *frame, deque<double> &angleBuffer, bool ignoreLeft = false);

Point pOrigin;
void setFloorHistogram ();

log4cpp::Category* root;

const float CAM_FOV = 15.0;
const int FRAME_WIDTH 	= 320;
const int FRAME_HEIGHT 	= 240;
const int NO_COLOR		= -2147483647;
const int STOP		= -2147483643;

//sensors
const int SENSOR_DISTANCE_TO_TABLE = 1;
const int SENSOR_DISTANCE_ROTATE_1 = 2;
const int SENSOR_DISTANCE_ROTATE_2 = 3;
const int SENSOR_TOUCH_CLOSE = 4;
const int SENSOR_TOUCH_ROTATE = 5;
const int SENSOR_TOUCH_TABLE = 6;


const int FCT_ROTATE = 1233;
const int FCT_ROTATE_TABLE = 1234;
const int FCT_GO_TO_TRAY = 1235;

CvHistogram* hist;
Color targetColorLower = {100, 135, 135};
Color targetColorUpper={140, 255, 255};

//Color targetColorLower = { 56, 114, 79};
//Color targetColorUpper={ 90, 237, 211};


IplImage *frame, *huePlane, *satPlane, *valPlane, *imgHSV, *backproj, *smooth,*imgThresh;

Mat src,src_gray;
int hdims[3] = {12, 8, 4};
float **hranges;
IplImage** all_channels;
int yThreshold, key;
byte createIsRunning = 0;

int ALLOWED_MISSES_CONST = 250;
int allowedMisses = 0;
int lastFoundPointX, lastFoundPointY;
char ARDUINO_PORT[] = "/dev/ttyACM0";

void close_cameras (int param)
{
    if(camera1 != NULL){
        root->info("close camera 1");
        cvReleaseCapture(&camera1);
    }

    if(camera2 != NULL){
        root->info("close camera 2");
        cvReleaseCapture(&camera2);
    }

    root->info("stop NXT");
    NXT::Motor::Stop(OUT_A, false);
    NXT::Motor::Stop(OUT_B, false);
    NXT::Motor::Stop(OUT_C, false);
    root->info("stop irobot");
    drive(0,0);

    exit(-5);
}

bool comp (const int& first, int& second) {
  return first < second;
}

bool comp2(const double& first, const double& second) {
    return first < second;
}

inline double convertToRadians(double degrees){
    return degrees * M_PI / 180;
}

int getDistance(Distance d, int sensor, deque<int>& coada) {
            usleep(10 * 1000);
            int dist = d.read(sensor);
            coada.push_back(dist);
            if (coada.size() > DEQUE_SIZE) {
                coada.pop_front();
            }
            list<int> l(coada.begin(), coada.end());
            l.sort(comp);

            long suma = 0;
            int i = 0;
            for (std::list<int>::iterator it=l.begin(); it != l.end(); ++it) 
            {
                i++;
                if(i == 5) {
                    return *it;
                }
                suma += (*it);
            }
            suma -= l.front();
            suma -= l.back();

            dist = suma / (l.size() - 2);
            return dist;
}

void go(Distance d, int distance) {
    deque<int> coada; 

    while (coada.size() < DEQUE_SIZE) {
        int dist = d.read(SENSOR_DISTANCE_TO_TABLE);
        coada.push_back(dist);
        usleep(10 * 1000);
    }

    if (d.read(SENSOR_DISTANCE_TO_TABLE) > distance) {
        drive(100, 0);
        bool ok = true;
        while(ok) {
            int dist = getDistance(d, SENSOR_DISTANCE_TO_TABLE, coada);

            root->info("Distance to table: %d\n", dist);
            ok = dist > distance;
        }
    } else {
        usleep(10 * 1000);

        drive(-100, 0);
        bool ok = true;
        while(ok) {
            int dist = getDistance(d, SENSOR_DISTANCE_TO_TABLE, coada);

            root->info("Distance to table: %d\n", dist);
            ok = dist < distance;
        }
    }
    drive(0, 0);
}

void moveGripperUp(int rotation) {
    NXT::Motor::SetReverse(OUT_A, 80);
    while(-rotation < NXT::Motor::GetRotationCount(OUT_A)) {
	root->info("Rotation %d rotations: %d", rotation, NXT::Motor::GetRotationCount(OUT_A));
    }
    NXT::Motor::Stop(OUT_A, false);
}

void moveGripperDown(Distance d, int rotation) {
    NXT::Motor::SetForward(OUT_A, 80);
    if(rotation != 0) {
        while(rotation > NXT::Motor::GetRotationCount(OUT_A)) {}
    } else {
        while(d.read(SENSOR_TOUCH_TABLE) != 1) {}    
    }
    root->info("Rotation till down: %d", NXT::Motor::GetRotationCount(OUT_A));
    NXT::Motor::Stop(OUT_A, false);
}

IplImage* GetThresholdedImage(IplImage* imgHSV) {       
       IplImage* imgThresh=cvCreateImage(cvGetSize(imgHSV),IPL_DEPTH_8U, 1);
       cvInRangeS(imgHSV, cvScalar(targetColorLower.hue, targetColorLower.saturation, targetColorLower.value),
       cvScalar(targetColorUpper.hue, targetColorUpper.saturation, targetColorUpper.value), imgThresh);
       return imgThresh;
} 


int turns(cv::Point p0, cv::Point p1, cv::Point p2) {
    return (p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y);
}

std::vector<cv::Point> extractPoints(std::vector<Vec4i> lines) {
    std::vector<cv::Point> points;
    for(unsigned int i = 0; i < lines.size(); i++){
        points.push_back(cv::Point(lines[i][0], lines[i][1]));
        points.push_back(cv::Point(lines[i][2], lines[i][3]));
    }
    return points;
}

int cmp(Point p1, Point p2) { 
    return turns(pOrigin, p1, p2) < 0;
}

std::vector<cv::Point> executeGrahamScan(std::vector<cv::Point> points) {
    std::vector<cv::Point> convexHullPoints;
    if(points.size() < 3) {
        return convexHullPoints;
    }

    //extract origin
    uint pmin = 0;
    for(uint i = 0; i < points.size(); i++){
        if(points[i].y < points[pmin].y){
            pmin = i;
        }
    }

    swap(points[0], points[pmin]);
    pOrigin = points[0];

    //sort points
    sort(++(points.begin()), points.end(), cmp);
    
    //Graham scan algorithm
    convexHullPoints.push_back(points[0]);
    convexHullPoints.push_back(points[1]);

    for (uint i = 2; i < points.size(); ++i) {
        while (convexHullPoints.size()>=2 && turns(convexHullPoints[convexHullPoints.size() - 2], 
                                              convexHullPoints[convexHullPoints.size() -1], 
                                              points[i])>0 ) {
            convexHullPoints.pop_back();
        }
        convexHullPoints.push_back(points[i]);
    }
    return convexHullPoints;
}

std::vector<Vec4i> extractConvexHullLines(std::vector<cv::Point> convexHullPoints) {
    std::vector<Vec4i> lines;

    root->info("Extract lines. Points = %d", convexHullPoints.size());
    if(convexHullPoints.size() > 1){
        for(uint i = 0; i < convexHullPoints.size() - 1; i++) {
            lines.push_back(Vec4i(convexHullPoints[i].x, convexHullPoints[i].y, convexHullPoints[i+1].x, convexHullPoints[i+1].y));
        }

        uint last = convexHullPoints.size() - 1;
        lines.push_back(Vec4i(convexHullPoints[last].x, convexHullPoints[last].y, convexHullPoints[0].x, convexHullPoints[0].y));
    }

    return lines;
}

inline double distanceBetween2Points(Point &p1, Point &p2) {
    return sqrt( (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y)  );
}

inline bool areAlmostPerpendicular(LineInfo line1, LineInfo line2) {
    double maxError = 10.00;

    double angle = line1.angle - line2.angle;
    root->info("angle: %f", angle);

    if(angle < 0) {
        angle = line2.angle - line1.angle;
        root->info("angle: %f", angle);
    }

    if(angle >= 90 - maxError && angle <= 90 + maxError) {
        return true;
    }
    return false;
}

double computeAngleOfLine(Point p1, Point p2) {
    Point firstPoint = p1;
    Point secondPoint = p2;

    if(firstPoint.y > secondPoint.y) {
        firstPoint = p2;
        secondPoint = p1;
    }

    double angle = atan2((double)secondPoint.y - firstPoint.y,
                        (double)secondPoint.x - firstPoint.x) * 180 / CV_PI;
    return angle;
}

double extractPreciseAngle(deque<double> &angles, double angle) {
    double mean;

    angles.push_back(angle);
    if (angles.size() > ANGLE_DEQUE_SIZE) {
        angles.pop_front();
    }

    list<double> l(angles.begin(), angles.end());
    l.sort(comp2);

    long double suma = 0;
    for (std::list<double>::iterator it=l.begin(); it != l.end(); ++it) 
    {
        suma += (*it);
    }
    suma -= l.front();
    suma -= l.back();

    mean = suma / (l.size() - 2);
    return mean;
}

double extractAngle(std::vector<Vec4i> lines) {
    std::vector<LineInfo> lineInfos;

    for(unsigned int i = 0; i < lines.size(); i++) {
        LineInfo lineInfo;
        lineInfo.p1 = Point(lines[i][0], lines[i][1]);
        lineInfo.p2 = Point(lines[i][2], lines[i][3]);
        lineInfo.length = distanceBetween2Points(lineInfo.p1, lineInfo.p2);
        lineInfo.angle = computeAngleOfLine(lineInfo.p1, lineInfo.p2);
        lineInfos.push_back(lineInfo);
    }

    /*
    uint max = 0;

    for(uint i = 1; i < lineInfos.size(); i++) {
        if(lineInfos[max].length < lineInfos[i].length) {
            max = i;
        }
    }

    return lineInfos[max].angle;
    */

    uint min = 0;
    for(uint i = 1; i< lineInfos.size();i++){
        if(abs(lineInfos[min].angle - 90) > abs(lineInfos[i].angle - 90)){
            min = i;
        }
    }

    return lineInfos[min].angle;
}

bool caughtTrayInImage(std::vector<Vec4i> lines, double precision, bool ignoreLeft) {
    int nrI = 0;
    int offset = 3;

    if(lines.size() == 0){
        return false;
    }

    for(unsigned int i = 0; i < lines.size(); i++) {
        if((ignoreLeft || lines[i][0] >= offset) && lines[i][0] <= FRAME_WIDTH - offset && (ignoreLeft || lines[i][1] >= offset) && lines[i][1] <= FRAME_HEIGHT) {
            nrI++;
        }
        if((ignoreLeft || lines[i][2] >= offset) && lines[i][2] <= FRAME_WIDTH - offset && (ignoreLeft || lines[i][3] >= offset) && lines[i][3] <= FRAME_HEIGHT) {
            nrI++;
        }
    }
    if(nrI / (lines.size() * 2) * 100 < precision) {
        return false;
    }
    return true;
}

void closeGripper(Distance d) {
    NXT::Motor::ResetRotationCount(OUT_C, false);
    NXT::Motor::SetReverse(OUT_C, 50);
    root->info("Closing gripper");
    while(d.read(SENSOR_TOUCH_CLOSE) != 1) {}
    NXT::Motor::Stop(OUT_C, false);
    root->info("Closed gripper");
	
}

void openGripper(Distance d) {
    int rotation = NXT::Motor::GetRotationCount(OUT_C);
    root->info("Open gripper %d\n", rotation);
    NXT::Motor::ResetRotationCount(OUT_C, false);
    NXT::Motor::SetForward(OUT_C, 50);
    while(rotation + 100  < - NXT::Motor::GetRotationCount(OUT_C)){
    }
    NXT::Motor::Stop(OUT_C, false);
}

void rotateGripperRight(Distance d) {
    NXT::Motor::ResetRotationCount(OUT_B, false);
    NXT::Motor::SetReverse(OUT_B, 50);
    while(d.read(SENSOR_TOUCH_ROTATE) != 0) {}
    NXT::Motor::Stop(OUT_B, false);
}

void rotateGripperLeft(int rotation, Distance d) {
    NXT::Motor::ResetRotationCount(OUT_B, false);
    NXT::Motor::SetForward(OUT_B, 50);
    while(rotation > NXT::Motor::GetRotationCount(OUT_B)){}
    NXT::Motor::Stop(OUT_B, false);
}

void goToTable(Distance d) {
    go(d, DISTANCE_TO_TABLE + 10);
    turn (40, -1, -90, 1);
}

void initDeque2(deque<double> &coada){
    for(uint i = 0; i < ANGLE_DEQUE_SIZE; i++){
        coada.push_back(45);
    }
}

Direction getRobotDirection(int code){
    switch(code){
        case 248:
        case 250:
            return LEFT;
        case 244:
        case 246:
            return RIGHT;
        case 252:
        case 254:
            return AHEAD;
    }

    return ROTATE;
}

void getTray(CvCapture* camera1, CvCapture* camera2, Distance d) {
    moveGripperDown(d, 0);
    closeGripper(d);
    moveGripperUp(20);
    //go to another table
    go(d, 80);
    turn(30, 1, 120, 1);
    drive(80, 0);
    RoboState roboState = STAND_BY;
    double angleToRotate;
    int ANGLE_ROTATE_STEP = 2;
    int rotationSpeed = 40;
    int rotationScale = 2.5;

    bool ok = true;
    deque<int> coada; 

    while (coada.size() < DEQUE_SIZE) {
        int dist = d.read(SENSOR_DISTANCE_TO_TABLE);
        coada.push_back(dist);
        usleep(10 * 1000);
    }
    while(ok) {
        int dist = getDistance(d, SENSOR_DISTANCE_TO_TABLE, coada);

        root->info("Distance to table: %d\n", dist);
        ok = dist > 50;

        int sensorValue = readSensor ((oi_sensor)17);
        Direction roboDir = getRobotDirection(sensorValue);

        switch(roboDir){
            case AHEAD:
                root->info("RoboDir: AHEAD");
                roboState = IN_MARCH;
                break;
            case LEFT:
                root->info("RoboDir: LEFT");
                roboState = IN_LEFT_ROTATION;
                break;
            case RIGHT:
                root->info("RoboDir: RIGHT");
                roboState = IN_RIGHT_ROTATION;
                break;
            default:
                root->info("RoboDir: WONDER_AROUND");
                //rotate
                switch(roboState){
                    case STAND_BY:
                        root->info("State: STAND_BY");
                        roboState = IN_MINUS_180_ROTATION;
                        angleToRotate = -180;
                        break;
                    case IN_MINUS_180_ROTATION:
                        root->info("Angle %f, State: IN_MINUS_180_ROTATION", angleToRotate);
                        if(abs(angleToRotate) < ANGLE_ROTATE_STEP){
                            roboState = IN_PLUS_360_ROTATION;
                            angleToRotate = 360;
                        }
                        break;
                    case IN_PLUS_360_ROTATION:
                        root->info("Angle %f, State: IN_PLUS_360_ROTATION", angleToRotate);
                        if(abs(angleToRotate) < ANGLE_ROTATE_STEP){
                            roboState = STAND_BY;
                            angleToRotate = 0;
                        }
                        break;
                    case IN_LEFT_ROTATION:
                        root->info("State: IN_LEFT_ROTATION");
                        break;
                    case IN_RIGHT_ROTATION:
                        root->info("State: IN_RIGHT_ROTATION");
                        break;
                    case IN_MARCH:
                        root->info("State: IN_MARCH");
                        roboState = STAND_BY;
                        break;
                }
                break;
        }

        switch(roboState){
            case STAND_BY:
                break;
            case IN_LEFT_ROTATION:
                directDrive(rotationSpeed, rotationSpeed * rotationScale);
                break;
            case IN_RIGHT_ROTATION:
                directDrive(rotationSpeed * rotationScale, rotationSpeed);
                break;
            case IN_MARCH:
                directDrive(rotationSpeed, rotationSpeed);
                break;
            case IN_MINUS_180_ROTATION:
                drive(0, 0);
                turn (rotationSpeed, 1, ANGLE_ROTATE_STEP, 1);
                angleToRotate += ANGLE_ROTATE_STEP;
                break;
            case IN_PLUS_360_ROTATION:
                drive(0, 0);
                turn (rotationSpeed, -1, -ANGLE_ROTATE_STEP, 1);
                angleToRotate -= ANGLE_ROTATE_STEP;
                break;
        }
    }

    root->info("Rotate Gripper");
    go(d, 35);
    turn(rotationSpeed, 1, 60, 1);
    rotateGripperLeft(500, d);
    rotateGripperRight(d);
    turn(rotationSpeed, -1, -60, 1);
    go(d, 35);
    moveGripperDown(d, 0);
    openGripper(d);
    moveGripperUp(20);
    go(d, 60);
    turn(50, -1, -180, 1);
}

deque<int> initDeque(deque<int> coada, Distance d, int sensor) {
    while (coada.size() < DEQUE_SIZE) {
        int dist = d.read(sensor);
        coada.push_back(dist);
        usleep(10 * 1000);
    }
    return coada;
}

int adjustGoToTray = 0;

bool finished2(CvCapture* camera, int fct, deque<int> coada1, deque<int> coada2) {
    deque<double> angleBuffer;
    //initDeque2(angleBuffer);

    Point weightCenter;
    frame = cvQueryFrame (camera);
    ComputeResult angleData = computeAngleData(frame, angleBuffer);

    int angleerror = 10;
    bool ok = false;

    if(fct == FCT_ROTATE && FRAME_WIDTH / 2 - angleerror + 5 < angleData.weightCenter.x && angleData.weightCenter.x < FRAME_WIDTH / 2 + angleerror + 5) {
        ok = true;
        root->info("caught in tray OK = true;");
    }

    root->info("angle = %f", angleData.angle);

    if(fct == FCT_GO_TO_TRAY){
        if(angleData.weightCenter.x < FRAME_WIDTH / 2){
            adjustGoToTray = 1;
        }else{
            adjustGoToTray  = -1;
        }
    }

    if(fct == FCT_GO_TO_TRAY && FRAME_HEIGHT / 2 < angleData.weightCenter.y && angleData.weightCenter.y < FRAME_HEIGHT / 2 + 30) {
        root->info("FCT_GO_TO_TRAY");
        ok = true;
    }

    if(ok) {
        root->info("FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED FINISHED");
        return true;
    }

    key = cvWaitKey (10) & 255;
    if (27 == key) {
        return true;
    }
    
    root->info("return false");
    return false;
}

int sign(double nr){
    if(nr<0){
        return -1;
    }

    return 1;
}

Point translatePointWithAngle(Point point, double angle){
    Point newPoint = point;

    if(angle > 0 && angle < 90) {
        root->info("alter X = %f", tan(convertToRadians(abs(angle))) * (FRAME_HEIGHT - point.y));
        newPoint.x -= tan(convertToRadians(abs(angle))) * (FRAME_HEIGHT - point.y);
    } else  if(angle > -90 && angle < 0) {
        root->info("alter X = %f", tan(convertToRadians(abs(angle))) * (FRAME_HEIGHT - point.y));
        newPoint.x += tan(convertToRadians(abs(angle))) * (FRAME_HEIGHT - point.y);
    }

    newPoint.y = FRAME_HEIGHT;

    return newPoint;
}

ComputeResult computeAngleData(IplImage *frame, deque<double> &angleBuffer, bool ignoreLeft){
    ComputeResult response;
    response.foundAtLeast3Points = false;
    response.caughtTrayInImage = false;
    
    cvSmooth(frame, smooth, CV_MEDIAN, 11);
    cvCvtColor (smooth, imgHSV, CV_BGR2HSV);
    IplImage *temp = GetThresholdedImage(imgHSV);
    vector<Vec4i> lines;
    HoughLinesP(Mat(temp), lines, 1, CV_PI/180, 100, 10, 35);
    cv::Mat disp_lines(cvSize(frame->width, frame->height), CV_8UC1, cv::Scalar(0, 0, 0));
    cv::Mat finalLinesImage(cvSize(frame->width, frame->height), CV_8UC1, cv::Scalar(0, 0, 0));
    cv::Mat extraFinalLinesImage(cvSize(frame->width, frame->height), CV_8UC1, cv::Scalar(0, 0, 0));
    Mat finalBlur(cvSize(frame->width, frame->height), CV_8UC1, cv::Scalar(0, 0, 0));

    for(uint i = 0; i < lines.size(); i++){
        cv::line(disp_lines, cv::Point (lines[i][0],lines[i][1]), cv::Point(lines[i][2], lines[i][3]), cv::Scalar(255, 0, 0));
    }

    std::vector<cv::Point> points = extractPoints(lines);

    //points = extractPoints(lines);
        
    double totalX = 0, totalY = 0;
    for(uint i = 0; i < points.size(); i++) {
        totalX += points[i].x;
        totalY += points[i].y;
    }

    response.weightCenter.x = totalX / points.size();
    response.weightCenter.y = totalY / points.size();

    if(points.size() >= 3) {
        response.foundAtLeast3Points = true;
        points = executeGrahamScan(points);
    


        root->info("enter extractConvexHullLines");
        response.convexHullLines = extractConvexHullLines(points);
        root->info("exit extractConvexHullLines");

        if(response.convexHullLines.size() >= 1){
            for(uint i = 0; i < response.convexHullLines.size(); i++){
               cvLine(frame, cv::Point (response.convexHullLines[i][0], response.convexHullLines[i][1]), cv::Point(response.convexHullLines[i][2], response.convexHullLines[i][3]), cv::Scalar(230, 220, 15));
               cv::line(finalLinesImage, cv::Point (response.convexHullLines[i][0], response.convexHullLines[i][1]), cv::Point(response.convexHullLines[i][2], response.convexHullLines[i][3]), cv::Scalar(230, 220, 15), 5);
            }

            cvLine(frame, cv::Point (response.convexHullLines[0][0], response.convexHullLines[0][1]), cv::Point(response.convexHullLines[response.convexHullLines.size() - 1][2], response.convexHullLines[response.convexHullLines.size() - 1][3]), cv::Scalar(230, 220, 15));
            cv::line(finalLinesImage, cv::Point (response.convexHullLines[0][0], response.convexHullLines[0][1]), cv::Point(response.convexHullLines[response.convexHullLines.size() - 1][2], response.convexHullLines[response.convexHullLines.size() - 1][3]), cv::Scalar(230, 220, 15), 5);
        }

        root->info("print lines on image");
        medianBlur(finalLinesImage, finalBlur, 3);
        root->info("medianBlur");
        lines.clear();
        HoughLinesP(finalBlur, lines, 1, CV_PI/180, 100, 10, 35);
        root->info("hough lines 2");

        cvCircle(frame, response.weightCenter, 10, CV_RGB(0,255,0), -1);

        if(lines.size() > 0){
            for(uint i = 0; i < lines.size();i++){
                cv::line(extraFinalLinesImage, cv::Point (lines[i][0], lines[i][1]), cv::Point(lines[i][2], lines[i][3]), cv::Scalar(230, 220, 15), 4);
            }

            int index = 0;
            Point p1(lines[0][0], lines[0][1]);
            Point p2(lines[0][2], lines[0][3]);
            double maxLength = distanceBetween2Points(p1, p2);

            for(uint i = 1; i < lines.size(); i++) {
                Point p3(lines[i][0], lines[i][1]);
                Point p4(lines[i][2], lines[i][3]);
                double length = distanceBetween2Points(p3, p4);
                if(length > maxLength){
                    maxLength = length;
                    index = i;
                }
            }

            cv::line(extraFinalLinesImage, Point(lines[index][0], lines[index][1]), Point(lines[index][2], lines[index][3]), cv::Scalar(0, 250, 0), 4);
        }

        root->info("before caught tray in image");
        if(caughtTrayInImage(lines, 10, ignoreLeft)) {
            root->info("inside if caught tray in image");
            response.caughtTrayInImage = true;
            response.angle = extractAngle(lines);
            response.angle -= 90;
            response.angle = extractPreciseAngle(angleBuffer, response.angle);

            double angleOffsetToCenter = 40;
            Point oldPoint = response.weightCenter;
            Point oldPointWithOffset = response.weightCenter;
            oldPointWithOffset.x += angleOffsetToCenter;
            
            Point newPoint = translatePointWithAngle(oldPoint, response.angle);
            Point newPointWithOffset = translatePointWithAngle(oldPointWithOffset, response.angle);
            
            cvLine(frame, oldPoint, newPoint, cv::Scalar(0, 250, 0));
            cvLine(frame, oldPointWithOffset, newPointWithOffset, cv::Scalar(0, 0, 250));
        }

        root->info("after caughtTrayInImage");
    }

    cvShowImage("real", frame);
    moveWindow("real", 0, 0);

    cvShowImage("threshold", temp);
    moveWindow("threshold", 0, FRAME_HEIGHT + 60);

    cv::imshow("lines", disp_lines);
    moveWindow("lines", FRAME_WIDTH + 100, 0);

    cv::imshow("finalLinesImage", finalLinesImage);
    moveWindow("finalLinesImage", 2 * FRAME_WIDTH + 200, 0);

    cv::imshow("finalBlur", finalBlur);
    moveWindow("finalBlur", FRAME_WIDTH + 100, FRAME_HEIGHT + 60);

    cv::imshow("extrafinallines", extraFinalLinesImage);
    moveWindow("extrafinallines", 2 * FRAME_WIDTH + 200, FRAME_HEIGHT + 60);
    
    cvReleaseImage(&temp);

    return response ;
}

void rotateTable (CvCapture* camera, Distance d) {
    root->info("ROTATE TABLE +======================================================");
    int counter = 0;
    deque<int> coada1; 
    deque<int> coada2;
    deque<double> angleBuffer;
    //initDeque2(angleBuffer);
    coada1 = initDeque(coada1, d, SENSOR_DISTANCE_ROTATE_1);
    coada2 = initDeque(coada2, d, SENSOR_DISTANCE_ROTATE_2);
    bool ignoreLeft = true;

    double angle = 0;
    //double offsetAngle = 15;
    Point weightCenter;
    root->info("Start looking after tray");
    bool ok = true;
    while(1) {
        frame = cvQueryFrame (camera);

        ComputeResult angleData = computeAngleData(frame, angleBuffer, ignoreLeft);
        
        root->info("caughtTrayInImage = %d", angleData.caughtTrayInImage);

        if(angleData.caughtTrayInImage){
            angle = angleData.angle;
            weightCenter = angleData.weightCenter;
            double angleOffsetToCenter = 40;
            weightCenter.x += angleOffsetToCenter;
            root->info("Before Translation  =======X=%d && Y=%d", weightCenter.x, weightCenter.y);
            weightCenter = translatePointWithAngle(weightCenter, angle);
            root->info("After translation =======X=%d && Y=%d", weightCenter.x, weightCenter.y);

            //weightCenter points to the center of the tray.
            root->info("=======X=%d && Y=%d", weightCenter.x, weightCenter.y);
            root->info("angle = %f", angleData.angle);
            //if(- FRAME_WIDTH / 2 + weightCenter.x < -5) {

            if(FRAME_WIDTH / 2 - 30 < weightCenter.x && weightCenter.x < FRAME_WIDTH / 2) {
                counter ++;
            } else {
                counter = 0;
            }
            if(counter > 2) {
                root->info("ROTATE ROTATE ROTATE ROTATE ROTATE ROTATE ROTATE ROTATE ROTATE ROcautTATE ROTATE ROTATE");
                break;
            }
        }
            
        int dist1 = getDistance(d, SENSOR_DISTANCE_ROTATE_1, coada1);
        int dist2 = getDistance(d, SENSOR_DISTANCE_ROTATE_2, coada2);
        if(dist2 == 0) {
            dist2= 255;
        }
        root->info("Distance: dist1= %d dist2=%d", dist1, dist2);
        int offset = 80;
        while(abs(dist2 - dist1) > 5 && dist1 < offset && dist2 < offset) {
            root->info("Recorecting dist1= %d dist2=%d", dist1, dist2);
            drive(50, sign(dist1 - dist2));
            dist1 = getDistance(d, SENSOR_DISTANCE_ROTATE_1, coada1);
            dist2 = getDistance(d, SENSOR_DISTANCE_ROTATE_2, coada2);    
            if(dist2 == 0) {
                dist2 = 255;
            }
            ok =true;
        }
        drive(50, 0);
        if(dist1 < offset && dist2 < offset && dist1 > 50 && dist2 > 50) {
            root->info("======================================Start==============================");
            turn (50, 1, 90, 1);
            goToTable(d);
            root->info("======================================Stopped==============================");
        }
        if(dist2 > offset && dist1 > offset && ok) {
            drive(50, 0);
            sleep(1.5);
            root->info("Rotate dist1= %d dist2=%d", dist1, dist2);
            turn (50, 1, 65, 1);
            drive(50, 0);
            root->info("Finished Rotate");
            ok = false;
        }
        key = cvWaitKey (10) & 255;
        if (27 == key) {
            break;
        }
    }
    drive(0, 0);
}

bool finished (CvCapture* camera, int fct) {
    deque<int> coada1; 
    deque<int> coada2;
    return finished2 (camera, fct, coada1, coada2);
}

void rotate(CvCapture *camera) {
    drive(40, 1);
    while(1) {
        if(finished(camera, FCT_ROTATE)) {
            break;
        }
    }

    drive(0, 0);
}

void goToTray(CvCapture* camera) {
    double speed = 15;
    drive(speed, 0);
    while(1) {
        if(finished(camera, FCT_GO_TO_TRAY)) {
            root->info("break in goToTray");
            break;
        }

        if(adjustGoToTray > 0){
            root->info("drive to left");
            directDrive(speed, speed * 1.5);
        }else{
            root->info("drive to right");
            directDrive(speed * 1.5, speed);
        }
    }

    drive(0, 0);
}

void detectandGoToTray(CvCapture* camera1, CvCapture* camera2, Distance d) {
    rotateTable(camera1, d);
    root->info("ROTATE TABLE END =======================================");
    rotate(camera2);
    goToTray(camera2);
    // mx-095
}

void connectToCamera(CvCapture* camera, const char* name, bool first) {
    cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cvSetCaptureProperty (camera, CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);


    if(first) {
        cvMoveWindow(CAMERA1_NAME, 500, 0);
        //Grabs and returns a frame from camera or file
        frame = cvQueryFrame (camera);

        imgHSV = cvCreateImage (cvGetSize (frame), 8, 3);
        smooth = cvCreateImage (cvGetSize (frame), 8, 3);

        huePlane = cvCreateImage (cvGetSize (frame), 8, 1);
        satPlane = cvCreateImage (cvGetSize (frame), 8, 1);
        valPlane = cvCreateImage (cvGetSize (frame), 8, 1);
        backproj = cvCreateImage (cvGetSize (frame), 8, 1);
        all_channels = (IplImage**)malloc(3* sizeof(IplImage *));
        hranges = (float**)malloc(3* sizeof(float *));
        hranges[0] = (float*)malloc(3 * 2 * sizeof(float));
        int i;
        for(i = 1; i < 3; i++)
        {
            hranges[i] = hranges[0] + i * 2;
        }
        hranges[0][0] = 0;
        hranges[0][1] = 180;
        hranges[1][0] = 0;
        hranges[1][1] = 255;
        hranges[2][0] = 0;
        hranges[2][1] = 255;

        hist = cvCreateHist( 3, hdims, CV_HIST_ARRAY, hranges, 1 );
        all_channels[0] = huePlane;
        all_channels[1] = satPlane;
        all_channels[2] = valPlane;
        setFloorHistogram ();
    }
}

void testGripper(CvCapture* camera1, CvCapture* camera2, Distance d)  {
    go(d, 20);
    getTray(camera1, camera2, d);
}

void testDetect(CvCapture* camera1, CvCapture* camera2, Distance d)  {
    turn (40, -1, -90, 1);
    drive(-50, 0);
    sleep(3);
    drive(0, 0);
    sleep(1);
    detectandGoToTray(camera1, camera2, d);
}

void run(CvCapture* camera1, CvCapture* camera2, Distance d) {
    byte song[8] = {58, 16, 55, 16, 51, 16, 52, 32};
    writeSong(0,4, song);
    //sleep(1);

    //int counter = 2;
    //while(counter > 0) {
        goToTable(d);
        detectandGoToTray(camera1, camera2, d);
        getTray(camera1, camera2, d);
        //counter --;
    //}

    playSong(0);
    sleep(2);
}

void testDetectAngle(CvCapture *camera){
    IplImage *frame = cvQueryFrame (camera);
    deque<double> angleBuffer;
    initDeque2(angleBuffer);
    imgHSV = cvCreateImage (cvGetSize (frame), 8, 3);
    
    while(1){
        root->info("get frame");
        frame = cvQueryFrame (camera);
        
        root->info("compute data");
        ComputeResult result =  computeAngleData(frame, angleBuffer);
        root->info("compute data finished");

        root->info("angle = %f", result.angle);
        root->info("foundAtLeast3Points = %d", result.foundAtLeast3Points);
        root->info("caughtTrayInImage = %d", result.caughtTrayInImage);
        root->info("weightCenter = (%d, %d)", result.weightCenter.x, result.weightCenter.y);
        cvWaitKey(15);
    }
}

/** Main method */
int main(int argc, char* argv[])
{
    yThreshold = FRAME_HEIGHT / 4;

    //init logging
    std::string initFileName = "log4cpp.properties";
    log4cpp::PropertyConfigurator::configure(initFileName);

    root = &(log4cpp::Category::getRoot());

    //starts iRobot OpenInterface
    
    camera1 = camera2 = NULL;
    
    signal (SIGINT, close_cameras);
    signal (SIGHUP, close_cameras);
    signal (SIGSEGV, close_cameras);
    signal (SIGILL, close_cameras);
    signal (SIGFPE, close_cameras);
    signal (SIGQUIT, close_cameras);
    signal (SIGTERM, close_cameras);
    signal (SIGKILL, close_cameras);    

    root->info("print-1");
    if (0 == (camera1 = cvCaptureFromCAM (1)))
    {
        root->info ("Could not connect to camera1\n");
        return -1;
    } else {
        connectToCamera(camera1, CAMERA1_NAME, true);
        root->info("Connected to camera1");
    }
    if (0 == (camera2 = cvCaptureFromCAM (2)))
    {
        root->info ("Could not connect to camera 2\n");
        cvReleaseCapture (&camera1);
        return -2;
    } else {
        connectToCamera(camera2, CAMERA2_NAME, false);
        root->info("Connected to camera2");
    }

    if(NXT::Open()) {
        root->info ("Connected to NXT\n");
    } else {
        root->info ("Could not connect to NXT\n");
        //cvReleaseCapture (&camera1);
        //cvReleaseCapture (&camera2);

        //return -3;
    }
    Distance d(ARDUINO_PORT);
    int started=startOI ("/dev/ttyUSB0");
    if(started < 0)
    {
        root->info("could not start robo\n");
        cvReleaseCapture (&camera1);
        cvReleaseCapture (&camera2);

        return -4;
    }
    else
    {
        root->info("started\n");
    }

    if(argc > 1) {
        if(strcmp(argv[1], "1") == 0) {
            root->info("TEST DETECT");
            testDetect(camera1, camera2, d);
        } else if(strcmp(argv[1], "2") == 0) {
            root->info("TESTGRIPPER");
            testGripper(camera1, camera2, d);
        } else {
            root->info("RUN");
            run(camera1, camera2, d);
        } 
    } else {
        run(camera1, camera2, d);
    }
    
    d.close();
    stopOI();
    cvDestroyAllWindows();
    cvReleaseImage (&smooth);
    cvReleaseImage (&imgHSV);
    cvReleaseImage (&huePlane);
    cvReleaseImage (&satPlane);
    cvReleaseImage (&valPlane);
    cvReleaseImage (&backproj);
    cvReleaseHist (&hist);
    cvReleaseImage (&frame);
    cvReleaseCapture (&camera1);
    cvReleaseCapture (&camera2);
    NXT::Close();

    log4cpp::Category::shutdown();
    return 0;
}

/** \brief  Set color histogram for floor
 *
 * 	Uses the lower portion of an image to set the histogram for the floor.  The function just uses the lower
 *  portion of the image to find it since we can assume that the floor will always be down there.
 */
void setFloorHistogram ()
{
    CvRect rect = cvRect (FRAME_WIDTH / 3, FRAME_HEIGHT - yThreshold,
                          FRAME_WIDTH / 3, yThreshold);

    cvSetImageROI (huePlane, rect);
    cvSetImageROI (satPlane, rect);
    cvSetImageROI (valPlane, rect);
    cvCalcHist (all_channels, hist, 0, 0);
    cvNormalizeHist (hist, 1024);
    cvResetImageROI (huePlane);
    cvResetImageROI (satPlane);
    cvResetImageROI (valPlane);
}
