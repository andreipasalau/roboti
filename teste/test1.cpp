#include <stdio.h>
#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>

using namespace cv;


const int FRAME_WIDTH 	= 320;
const int FRAME_HEIGHT 	= 240;

const double PI = 3.141592653589793238463f;

Mat img1;
SurfFeatureDetector detector(400);
vector<KeyPoint> keypoints1;
Mat descriptors1;
FlannBasedMatcher matcher;
vector<DMatch> matches;
SurfDescriptorExtractor extractor;

void readme();
float angleBetween(const Point2f &v1, const Point2f &v2);
bool doDetection(IplImage *frame, Point2f &bottomHalf, float &angle);

/** @function main */
int main( int argc, char** argv )
{
  if( argc != 2 )
  { readme(); return -1; }
  
  CvCapture* camera1;
  if (0 == (camera1 = cvCaptureFromCAM (1)))
  {
	  std::cout << "camera error\n" ;
	  return 1;
  }
  cvSetCaptureProperty (camera1, CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);

  img1 = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  Mat img2; // = imread( argv[2], CV_LOAD_IMAGE_GRAYSCALE );
  
  IplImage *frame;  

  if( !img1.data /*|| !img_2.data*/ )
  { std::cout<< " --(!) Error reading images " << std::endl; return -1; }

  // detecting keypoints
    //SurfFeatureDetector detector(400);
    vector<KeyPoint> keypoints2;
    detector.detect(img1, keypoints1);

    // computing descriptors
    //SurfDescriptorExtractor extractor;
    Mat descriptors2;
    extractor.compute(img1, keypoints1, descriptors1);
    

    // matching descriptors
    //BFMatcher matcher(4);
    //FlannBasedMatcher matcher;
    //vector<DMatch> matches;
    

    // drawing the results
    //namedWindow("matches", 1);
    
  
  int key;
  
  while (1)
  {
	  frame = cvQueryFrame (camera1);
	  float angle = 9999;
	  Point2f bh;
	  if (doDetection(frame, bh, angle))
		printf("angle: %f\n", angle);
	  
	  key = cvWaitKey (10) & 255;
      if (27 == key) {
          break;
      }
  }

  //waitKey(0);
  
  cvReleaseCapture (&camera1);

  return 0;
  }
  
  bool sortPoint2fByY(const Point2f& v1, const Point2f& v2)
  {
	  return v1.y > v2.y;
  }
  
  bool doDetection(IplImage *frame, Point2f &bottomHalf, float &angle) 
  {
	  Mat img2(frame);
	  cvtColor(img2, img2, CV_BGR2GRAY);
	  vector<KeyPoint> keypoints2;
	  Mat descriptors2;
	  
	  detector.detect(img2, keypoints2);
	  
	  if (keypoints2.size() < 4)
			return false;
	  
	  extractor.compute(img2, keypoints2, descriptors2);
	  
	  matcher.match(descriptors1, descriptors2, matches);
	  
	  double max_dist = 0; double min_dist = 100;

	  //-- Quick calculation of max and min distances between keypoints
	  for( int i = 0; i < descriptors1.rows; i++ )
	  { double dist = matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	  }

	 // printf("-- Max dist : %f \n", max_dist );
	  //printf("-- Min dist : %f \n", min_dist );

	  //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	  std::vector< DMatch > good_matches;

	  for( int i = 0; i < descriptors1.rows; i++ )
	  { if( matches[i].distance < 3*min_dist )
		 { good_matches.push_back( matches[i]); }
	  }
	  

	 /* Mat img_matches;
	  drawMatches( img1, keypoints1, img2, keypoints2,
				   good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
				   vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );		   
*/
	  //-- Localize the object
	  std::vector<Point2f> obj;
	  std::vector<Point2f> scene;

	  for( int i = 0; i < good_matches.size(); i++ )
	  {
		//-- Get the keypoints from the good matches
		obj.push_back( keypoints1[ good_matches[i].queryIdx ].pt );
		scene.push_back( keypoints2[ good_matches[i].trainIdx ].pt );
	  }

	  Mat H = findHomography( obj, scene, CV_RANSAC );

	  //-- Get the corners from the image_1 ( the object to be "detected" )
	  std::vector<Point2f> obj_corners(4);
	  obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( img1.cols, 0 );
	  obj_corners[2] = cvPoint( img1.cols, img1.rows ); obj_corners[3] = cvPoint( 0, img1.rows );
	  std::vector<Point2f> scene_corners(4);

	  perspectiveTransform( obj_corners, scene_corners, H);
	  
	  std::sort(scene_corners.begin(), scene_corners.end(), sortPoint2fByY);

	  //-- Draw lines between the corners (the mapped object in the scene - image_2 )
	  
	  Point2f corner1 = scene_corners[0] + Point2f( img1.cols, 0), 
		corner2 = scene_corners[1] + Point2f( img1.cols, 0),
		corner3 = scene_corners[2] + Point2f( img1.cols, 0),
		corner4 = scene_corners[3] + Point2f( img1.cols, 0);
	  
	  /*line( img_matches, corner1, corner2, Scalar(0, 255, 0), 4 );
	  line( img_matches, corner2, corner3, Scalar( 0, 255, 0), 4 );
	  line( img_matches, corner3, corner4, Scalar( 0, 255, 0), 4 );
	  line( img_matches, corner4, corner1, Scalar( 0, 255, 0), 4 );

	  //-- Show detected matches
	  imshow( "Good Matches & Object detection", img_matches );*/

	//printf("corners: %f %f\n", corner1.y, corner2.y);	 
	  
	  if (abs(corner1.x - corner2.x) > 1 || abs(corner1.y - corner2.y) > 1)
	  {
		  angle = int(atan((corner1.y-corner2.y)/(corner2.x-corner1.x))*180/PI);
		  bottomHalf.x = (corner1.x + corner2.x) / 2;
		  bottomHalf.y = (corner1.y + corner2.y) / 2;
	  }	  else
		return false;
	  
	  return true;
  }
  
  float angleBetween(const Point2f &v1, const Point2f &v2)
{
    float len1 = sqrt(v1.x * v1.x + v1.y * v1.y);
    float len2 = sqrt(v2.x * v2.x + v2.y * v2.y);

    float dot = v1.x * v2.x + v1.y * v2.y;

    float a = dot / (len1 * len2);

    if (a >= 1.0)
        return 0.0;
    else if (a <= -1.0)
        return PI;
    else
        return acos(a); // 0..PI
}

  /** @function readme */
  void readme()
  { std::cout << " Usage: ./SURF_detector <img1>" << std::endl; }
